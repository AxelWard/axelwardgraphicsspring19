#ifndef __GLWIDGET__INCLUDE__
#define __GLWIDGET__INCLUDE__

#include <QGLWidget>
#include <QOpenGLWidget>
#include <QOpenGLFunctions_3_3_Core>
#include <QMouseEvent>
#include <glm/glm.hpp>
#include <vector>

#include "panel.h"
#include "platformManager.h"

#define GLM_FORCE_RADIANS

using glm::mat4;
using glm::vec2;
using glm::vec3;

class GLWidget : public QOpenGLWidget, protected QOpenGLFunctions_3_3_Core {
	Q_OBJECT

public:
	GLWidget(QWidget *parent = 0);
	~GLWidget();

	GLuint loadShaders(const char* vertf, const char* fragf);

protected:
	void initializeGL();
	void resizeGL(int w, int h);
	void paintGL();

	void mousePressEvent(QMouseEvent *event);
	void mouseMoveEvent(QMouseEvent *event);
	void mouseReleaseEvent(QMouseEvent *event);

	void resetView();
	void valueUpdate(int index, float value);

private:
	void initializeTerrain();
	void renderTerrain();

	void initializeMenu();
	void renderMenu();

	glm::vec3 pointOnVirtualTrackball(const glm::vec2 &pt);

	// Platform Items
	GLuint terrainProg;
	GLuint terrainVao;

	GLuint terrainPositionBuffer;
	GLuint terrainColorBuffer;
	GLuint terrainNormalBuffer;
	GLuint terrainIndexBuffer;

	GLint terrainPositionIndex;
	GLint terrainColorIndex;
	GLint terrainNormalIndex;

	GLint terrainProjMatrixLoc;
	GLint terrainViewMatrixLoc;
	GLint terrainModelMatrixLoc;
	GLint terrainLightLoc;
	GLint terrainLightColorLoc;
	GLint terrainLightIntenLoc;
	GLint terrainShineLoc;

	mat4 terrainProjMatrix;
	mat4 terrainViewMatrix;
	mat4 terrainModelMatrix;

	PlatformManager platforms;

	// Menu Items
	GLuint menuProg;
	GLuint menuVao;
	GLuint menuPositionBuffer;
	GLuint menuColorBuffer;

	GLint menuProjectionMatrixLoc;
	mat4 menuOrtho;

	std::vector<vec2> menuPts;
	std::vector<vec3> menuColors;

	Panel menuPanel;
	int activeMenuItem;

	int width;
	int height;

	int numPlatforms;
	int numShown;

	vec3 start;
	vec3 next;
};

#endif
