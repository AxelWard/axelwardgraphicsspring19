HEADERS += glwidget.h panel.h button.h slider.h platformManager.h platform.h
SOURCES += glwidget.cpp main.cpp panel.cpp button.cpp slider.cpp platformManager.cpp platform.cpp

QT += opengl designer
CONFIG -= app_bundle
CONFIG += console c++11
INCLUDEPATH += "../include"
INCLUDEPATH += $$PWD

RESOURCES += shaders.qrc