#version 330

in vec2 position;
in vec3 colorIn;

out vec3 color;

uniform mat4 ProjectionMatrix;

void main() {
  gl_Position = ProjectionMatrix * vec4(position.x, position.y, 0, 1);
  color = colorIn;
}