#ifndef __PLATFORMMANAGER__INCLUDE__
#define __PLATFORMMANAGER__INCLUDE__

#include <vector>
#include <glm/glm.hpp>

#include "platform.h"

using glm::vec3;
using std::vector;

class PlatformManager {
public:
	PlatformManager();
	~PlatformManager();

	vector<vec3> getPositionData();
	vector<vec3> getColorData();
	vector<vec3> getNormalData();
	vector<unsigned int> getIndiceData();

	void updateHeight(float newValue);
	void updateSpacing(float newValue);
	void updateSize(float newValue);

	void addPlatform(float x, float y, float z, float heightMultiplier, float size);

private:

	vector<Platform> platforms;
	float spacingMultiplier;
};

#endif