#include "platformManager.h"

#include <iostream>

using glm::vec3;

using namespace std;

PlatformManager::PlatformManager() {
}

PlatformManager::~PlatformManager() {
}

void PlatformManager::addPlatform(float x, float y, float z, float heightMultiplier, float size) {
	Platform temp = Platform(x, y, z, heightMultiplier, size);
	platforms.push_back(temp);
}

vector<vec3> PlatformManager::getPositionData() {
	vector<vec3> positions;

	for (unsigned int i = 0; i < platforms.size(); i++) {
		vector<vec3> temp = platforms[i].getPositions();

		for (int j = 0; j < 24; j++) {
			positions.push_back(temp[j]);
		}
	}

	return positions;
}

vector<vec3> PlatformManager::getColorData() {
	vector<vec3> colors;

	for (unsigned int i = 0; i < platforms.size(); i++) {
		vector<vec3> temp = platforms[i].getColors();

		for (int j = 0; j < 24; j++) {
			colors.push_back(temp[j]);
		}
	}

	return colors;
}

vector<vec3> PlatformManager::getNormalData() {
	vector<vec3> normals;

	for (unsigned int i = 0; i < platforms.size(); i++) {
		vector<vec3> temp = platforms[i].getNormals();

		for (int j = 0; j < 24; j++) {
			normals.push_back(temp[j]);
		}
	}

	return normals;
}

vector<unsigned int> PlatformManager::getIndiceData() {
	vector<unsigned int> indices;

	for (unsigned int i = 0; i < platforms.size(); i++) {
		vector<unsigned int> temp = platforms[i].getIndices();

		for (int j = 0; j < 36; j++) {
			indices.push_back(temp[j] + (i*24));
		}
	}

	return indices;
}


void PlatformManager::updateHeight(float newValue) {
	for (unsigned int i = 0; i < platforms.size(); i++) {
		platforms[i].updateHeightMultiplier((newValue-.5));
	}
}

void PlatformManager::updateSpacing(float newValue) {
	for (unsigned int i = 0; i < platforms.size(); i++) {
		float newX = platforms[i].getInitCenter().x;
		float newZ = platforms[i].getInitCenter().y;

		newX = newX * (1.0 + newValue);
		newZ = newZ * (1.0 + newValue);

		platforms[i].updatePosition(newX, newZ);
	}
}

void PlatformManager::updateSize(float newValue) {
	for (unsigned int i = 0; i < platforms.size(); i++) {
		platforms[i].updateSize(newValue + .5);
	}
}