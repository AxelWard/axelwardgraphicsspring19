#include "panel.h"

#include <vector>
#include <glm/glm.hpp>
#include "button.h"

using glm::vec2;
using glm::vec3;

Panel::Panel() {
	numButtons = 0;
	numSliders = 0;
}

Panel::~Panel() {

}

void Panel::addButton(vec2 tl, int w, int h, vec3 color) {
	Button temp = Button(tl, w, h, color);

	buttons.push_back(temp);

	numButtons++;
}

void Panel::addSlider(vec2 tl, int w, vec3 color, vec3 barColor) {
	Slider temp = Slider(tl, w, color, barColor);

	sliders.push_back(temp);

	numSliders++;
}

void Panel::clearButtons() {
	buttons.clear();
	numButtons = 0;
}

void Panel::clearSliders() {
	sliders.clear();
	numSliders = 0;
}

int Panel::mouseClick(int x, int y) {
	
	for (int i = 0; i < numButtons; i++) {
		if (buttons[i].mouseClick(x, y)) {

			return(i);
		}
	}

	for (int i = 0; i < numSliders; i++) {
		if (sliders[i].mouseClick(x, y)) {
			return(i + numButtons);
		}
	}

	return(-1);
}

void Panel::setMenuItemDown(int item) {
	if (item < numButtons) {
		buttons[item].buttonDown();
	}
	else {
		sliders[item - numButtons].sliderDown();
	}
}

void Panel::setMenuItemUp(int item) {
	if (item < numButtons) {
		buttons[item].buttonUp();
	}
	else {
		sliders[item - numButtons].sliderUp();
	}
}


//Button functions

vec2 Panel::getButtonStart(int index) {
	return(buttons[index].topLeft);
}

int Panel::getButtonWidth(int index) {
	return(buttons[index].width);
}

int Panel::getButtonHeight(int index) {
	return(buttons[index].height);
}

vec3 Panel::getButtonColor(int index) {
	return(buttons[index].color);
}


//Slider functions

float Panel::getSliderValue(int index) {
	return(sliders[index].sliderVal());
}

int Panel::getSliderPos(int index) {
	return(sliders[index].sliderPos);
}

void Panel::setSliderPos(int index, int newPos) {
	sliders[index].updatePos(newPos);
}

vec2 Panel::getSliderStart(int index) {
	return(sliders[index].topLeft);
}

int Panel::getSliderWidth(int index) {
	return(sliders[index].width);
}

vec3 Panel::getSliderColor(int index) {
	return(sliders[index].color);
}

vec3 Panel::getSliderBarColor(int index) {
	return(sliders[index].barColor);
}