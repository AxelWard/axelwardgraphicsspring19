#include "button.h"

#include <glm/glm.hpp>
#include <algorithm>

using glm::vec2;
using glm::vec3;
using std::max;

Button::Button(vec2 tl, int w, int h, vec3 inColor) {
	topLeft = tl;
	width = w;
	height = h;

	downColor = vec3(max(0.0, inColor.x - .2), max(0.0, inColor.y - .2), max(0.0, inColor.z - .2));
	upColor = inColor;

	color = upColor;
}

Button::~Button() {

}

bool Button::mouseClick(int x, int y) {

	if((x > topLeft.x) && (x < topLeft.x + width)) {
		if ((y > topLeft.y) && (y < topLeft.y + height)) {
			return true;
		}
	}

	return false;
}

void Button::buttonDown() {
	color = downColor;
}

void Button::buttonUp() {
	color = upColor;
}