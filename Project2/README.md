## **Instructions for Use**


*Red Button* - Reset View

*Green Slider* - Adjust Heights

*Blue Slider* - Adjust Spacing

*Purple Slider* - Adjust Size

*Red Slider* - Adjust Number of Platforms