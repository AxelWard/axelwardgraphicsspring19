#include "glwidget.h"
#include <iostream>

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <QTextStream>

#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

using glm::inverse;
using glm::vec2;
using glm::vec3;
using glm::mat4;
using glm::perspective;
using glm::normalize;
using glm::length;
using glm::cross;
using glm::dot;
using glm::rotate;
using glm::value_ptr;
using glm::lookAt;
using std::vector;

using namespace std;

GLWidget::GLWidget(QWidget *parent) : QOpenGLWidget(parent) {

	menuPanel = Panel();
	activeMenuItem = -1;
	menuOrtho = glm::ortho(0.0f, 640.0f, 480.0f, 0.0f);
	numPlatforms = 0;
	terrainModelMatrix = mat4(1.0f);
}

GLWidget::~GLWidget() {
}

void GLWidget::initializeTerrain() {
	glGenVertexArrays(1, &terrainVao);
	glBindVertexArray(terrainVao);

	glGenBuffers(1, &terrainPositionBuffer);
	glGenBuffers(1, &terrainColorBuffer);
	glGenBuffers(1, &terrainNormalBuffer);
	glGenBuffers(1, &terrainIndexBuffer);

	platforms = PlatformManager();

	platforms.addPlatform(0.0f, 0.0f, 0.0f, -.5f, 0.5f);
	numPlatforms++;
	platforms.addPlatform(3.0f, 0.2f, 0.0f, -.5f, 0.5f);
	numPlatforms++;
	platforms.addPlatform(-3.0f, 0.4f, 0.0f, -.5f, 0.5f);
	numPlatforms++;
	platforms.addPlatform(0.0f, 0.6f, 3.0f, -.5f, 0.5f);
	numPlatforms++;
	platforms.addPlatform(0.0f, 0.8f, -3.0f, -.5f, 0.5f);
	numPlatforms++;

	platforms.addPlatform(3.0f, 1.0f, 3.0f, -.5f, 0.5f);
	numPlatforms++;
	platforms.addPlatform(-3.0f, 0.9f, -3.0f, -.5f, 0.5f);
	numPlatforms++;
	platforms.addPlatform(-3.0f, 0.7f, 3.0f, -.5f, 0.5f);
	numPlatforms++;
	platforms.addPlatform(3.0f, 0.5f, -3.0f, -.5f, 0.5f);
	numPlatforms++;
	platforms.addPlatform(-6.0f, 0.3f, 0.0f, -.5f, 0.5f);
	numPlatforms++;

	platforms.addPlatform(6.0f, 0.1f, 0.0f, -.5f, 0.5f);
	numPlatforms++;

	numShown = 1;

	vector<vec3> pts = platforms.getPositionData();
	vector<vec3> colors = platforms.getColorData();
	vector<vec3> normals = platforms.getNormalData();
	vector<unsigned int> indices = platforms.getIndiceData();

	glBindBuffer(GL_ARRAY_BUFFER, terrainPositionBuffer);
	glBufferData(GL_ARRAY_BUFFER, pts.size() * sizeof(vec3), &pts[0], GL_DYNAMIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, terrainColorBuffer);
	glBufferData(GL_ARRAY_BUFFER, colors.size() * sizeof(vec3), &colors[0], GL_DYNAMIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, terrainNormalBuffer);
	glBufferData(GL_ARRAY_BUFFER, normals.size() * sizeof(vec3), &normals[0], GL_DYNAMIC_DRAW);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, terrainIndexBuffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(unsigned int), &indices[0], GL_DYNAMIC_DRAW);

	GLuint program = loadShaders(":/terrainVert.glsl", ":/terrainFrag.glsl");
	glUseProgram(program);
	terrainProg = program;

	glBindBuffer(GL_ARRAY_BUFFER, terrainPositionBuffer);
	terrainPositionIndex = glGetAttribLocation(program, "position");
	glEnableVertexAttribArray(terrainPositionIndex);
	glVertexAttribPointer(terrainPositionIndex, 3, GL_FLOAT, GL_FALSE, 0, 0);

	glBindBuffer(GL_ARRAY_BUFFER, terrainColorBuffer);
	terrainColorIndex = glGetAttribLocation(program, "color");
	glEnableVertexAttribArray(terrainColorIndex);
	glVertexAttribPointer(terrainColorIndex, 3, GL_FLOAT, GL_FALSE, 0, 0);

	glBindBuffer(GL_ARRAY_BUFFER, terrainNormalBuffer);
	terrainNormalIndex = glGetAttribLocation(program, "normal");
	glEnableVertexAttribArray(terrainNormalIndex);
	glVertexAttribPointer(terrainNormalIndex, 3, GL_FLOAT, GL_FALSE, 0, 0);

	terrainProjMatrixLoc = glGetUniformLocation(program, "projection");
	terrainViewMatrixLoc = glGetUniformLocation(program, "view");
	terrainModelMatrixLoc = glGetUniformLocation(program, "model");
	terrainLightLoc = glGetUniformLocation(program, "lightPos");
	terrainLightColorLoc = glGetUniformLocation(program, "lightCol");
	terrainLightIntenLoc = glGetUniformLocation(program, "lightIntensity");
	terrainShineLoc = glGetUniformLocation(program, "shininess");
}

void GLWidget::initializeMenu() {

	menuPanel.addButton(vec2(10, 0), 50, 50, vec3(1, 0, 0));

	menuPanel.addSlider(vec2(10, 60), 100, vec3(.9, .9, .9), vec3(0, 1, 0)); //height
	menuPanel.addSlider(vec2(10, 90), 100, vec3(.9, .9, .9), vec3(0, 0, 1)); 
	menuPanel.addSlider(vec2(10, 120), 100, vec3(.9, .9, .9), vec3(1, 0, 1));
	menuPanel.addSlider(vec2(10, 150), 100, vec3(.9, .9, .9), vec3(1, 0, 0));

	glGenVertexArrays(1, &menuVao);
	glBindVertexArray(menuVao);

	glGenBuffers(1, &menuPositionBuffer);
	glGenBuffers(1, &menuColorBuffer);

	glBindBuffer(GL_ARRAY_BUFFER, menuPositionBuffer);
	glBufferData(GL_ARRAY_BUFFER, 0, NULL, GL_DYNAMIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, menuColorBuffer);
	glBufferData(GL_ARRAY_BUFFER, 0, NULL, GL_DYNAMIC_DRAW);

	GLuint program = loadShaders(":/menuVert.glsl", ":/menuFrag.glsl");
	glUseProgram(program);
	menuProg = program;

	glBindBuffer(GL_ARRAY_BUFFER, menuPositionBuffer);
	GLint positionIndex = glGetAttribLocation(program, "position");
	glEnableVertexAttribArray(positionIndex);
	glVertexAttribPointer(positionIndex, 2, GL_FLOAT, GL_FALSE, 0, 0);

	glBindBuffer(GL_ARRAY_BUFFER, menuColorBuffer);
	GLint colorIndex = glGetAttribLocation(program, "colorIn");
	glEnableVertexAttribArray(colorIndex);
	glVertexAttribPointer(colorIndex, 3, GL_FLOAT, GL_FALSE, 0, 0);

	menuProjectionMatrixLoc = glGetUniformLocation(program, "ProjectionMatrix");

	glUniformMatrix4fv(menuProjectionMatrixLoc, 1, GL_FALSE, glm::value_ptr(menuOrtho));
}

void GLWidget::initializeGL() {
	initializeOpenGLFunctions();

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glPointSize(4.0f);

	glEnable(GL_DEPTH_TEST);

	initializeTerrain();
	initializeMenu();
}

void GLWidget::resizeGL(int w, int h) {
	width = w;
	height = h;

	float aspect = (float)w / h;

	terrainViewMatrix = lookAt(vec3(0, 0, -15), vec3(0, 0, 0), vec3(0, 1, 0));
	terrainProjMatrix = perspective(45.0f, aspect, 1.0f, 100.0f);
	
	menuOrtho = glm::ortho(0.0f, (float) w, (float) h, 0.0f);

	float lightPos[]{ 0, 10, 0};
	float lightColor[]{ 1, 1, 1 };
	float lightIntensity = 1;
	float shininess = 50;

	glUseProgram(terrainProg);
	glUniformMatrix4fv(terrainProjMatrixLoc, 1, false, value_ptr(terrainProjMatrix));
	glUniformMatrix4fv(terrainViewMatrixLoc, 1, false, value_ptr(terrainViewMatrix));
	glUniformMatrix4fv(terrainModelMatrixLoc, 1, false, value_ptr(terrainModelMatrix));
	glUniform3fv(terrainLightLoc, 1, lightPos);
	glUniform3fv(terrainLightColorLoc, 1, lightColor);
	glUniform1f(terrainLightIntenLoc, lightIntensity);
	glUniform1f(terrainShineLoc, shininess);

	glUseProgram(menuProg);
	glUniformMatrix4fv(menuProjectionMatrixLoc, 1, GL_FALSE, glm::value_ptr(menuOrtho));
}

void GLWidget::paintGL() {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	renderTerrain();
	renderMenu();
}

void GLWidget::renderTerrain() {
	glUseProgram(terrainProg);
	glBindVertexArray(terrainVao);

	vector<vec3> pts = platforms.getPositionData();
	vector<vec3> colors = platforms.getColorData();
	vector<vec3> normals = platforms.getNormalData();
	vector<unsigned int> indices = platforms.getIndiceData();

	glBindBuffer(GL_ARRAY_BUFFER, terrainPositionBuffer);
	glBufferData(GL_ARRAY_BUFFER, pts.size() * sizeof(vec3), &pts[0], GL_DYNAMIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, terrainColorBuffer);
	glBufferData(GL_ARRAY_BUFFER, colors.size() * sizeof(vec3), &colors[0], GL_DYNAMIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, terrainNormalBuffer);
	glBufferData(GL_ARRAY_BUFFER, normals.size() * sizeof(vec3), &normals[0], GL_DYNAMIC_DRAW);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, terrainIndexBuffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(unsigned int), &indices[0], GL_DYNAMIC_DRAW);

	glDrawElements(GL_TRIANGLES, numShown * 36, GL_UNSIGNED_INT, 0);
}

void GLWidget::renderMenu() {
	glUseProgram(menuProg);
	glBindVertexArray(menuVao);

	int num_buttons = menuPanel.numButtons;
	int num_sliders = menuPanel.numSliders;

	for (int i = 0; i < num_buttons; i++) {

		vec2 start = menuPanel.getButtonStart(i);
		int w = menuPanel.getButtonWidth(i);
		int h = menuPanel.getButtonHeight(i);
		vec3 color = menuPanel.getButtonColor(i);

		menuPts.push_back(start);
		menuColors.push_back(color);

		menuPts.push_back(vec2(start.x, start.y + h));
		menuColors.push_back(color);

		menuPts.push_back(vec2(start.x + w, start.y));
		menuColors.push_back(color);

		menuPts.push_back(vec2(start.x, start.y + h));
		menuColors.push_back(color);

		menuPts.push_back(vec2(start.x + w, start.y + h));
		menuColors.push_back(color);

		menuPts.push_back(vec2(start.x + w, start.y));
		menuColors.push_back(color);
	}

	for (int i = 0; i < num_sliders; i++) {

		vec2 start = menuPanel.getSliderStart(i);
		int w = menuPanel.getSliderWidth(i);
		int h = 25;
		vec3 barColor = menuPanel.getSliderBarColor(i);
		vec3 color = menuPanel.getSliderColor(i);
		int pos = menuPanel.getSliderPos(i);

		//button

		menuPts.push_back(vec2(pos - 5, start.y));
		menuColors.push_back(color);

		menuPts.push_back(vec2(pos - 5, start.y + h));
		menuColors.push_back(color);

		menuPts.push_back(vec2(pos + 5, start.y));
		menuColors.push_back(color);

		menuPts.push_back(vec2(pos + 5, start.y));
		menuColors.push_back(color);

		menuPts.push_back(vec2(pos + 5, start.y + h));
		menuColors.push_back(color);

		menuPts.push_back(vec2(pos - 5, start.y + h));
		menuColors.push_back(color);

		//bar

		menuPts.push_back(vec2(start.x, start.y + 11));
		menuColors.push_back(barColor);

		menuPts.push_back(vec2(start.x, start.y + 15));
		menuColors.push_back(barColor);

		menuPts.push_back(vec2(start.x + w, start.y + 15));
		menuColors.push_back(barColor);

		menuPts.push_back(vec2(start.x, start.y + 11));
		menuColors.push_back(barColor);

		menuPts.push_back(vec2(start.x + w, start.y + 15));
		menuColors.push_back(barColor);

		menuPts.push_back(vec2(start.x + w, start.y + 11));
		menuColors.push_back(barColor);
	}

	glBindBuffer(GL_ARRAY_BUFFER, menuPositionBuffer);
	glBufferData(GL_ARRAY_BUFFER, menuPts.size() * sizeof(vec2), &menuPts[0], GL_DYNAMIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, menuColorBuffer);
	glBufferData(GL_ARRAY_BUFFER, menuColors.size() * sizeof(vec3), &menuColors[0], GL_DYNAMIC_DRAW);

	glDrawArrays(GL_TRIANGLES, 0, ((menuPanel.numButtons * 6) + (menuPanel.numSliders *12)));
	
	menuPts.clear();
	menuColors.clear();
}

GLuint GLWidget::loadShaders(const char* vertf, const char* fragf) {
	GLuint program = glCreateProgram();

	// read vertex shader from Qt resource file
	QFile vertFile(vertf);
	vertFile.open(QFile::ReadOnly | QFile::Text);
	QString vertString;
	QTextStream vertStream(&vertFile);
	vertString.append(vertStream.readAll());
	std::string vertSTLString = vertString.toStdString();

	const GLchar* vertSource = vertSTLString.c_str();

	GLuint vertShader = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vertShader, 1, &vertSource, NULL);
	glCompileShader(vertShader);
	{
		GLint compiled;
		glGetShaderiv(vertShader, GL_COMPILE_STATUS, &compiled);
		if (!compiled) {
			GLsizei len;
			glGetShaderiv(vertShader, GL_INFO_LOG_LENGTH, &len);

			GLchar* log = new GLchar[len + 1];
			glGetShaderInfoLog(vertShader, len, &len, log);
			std::cout << "Shader compilation failed: " << log << std::endl;
			delete[] log;
		}
	}
	glAttachShader(program, vertShader);

	// read fragment shader from Qt resource file
	QFile fragFile(fragf);
	fragFile.open(QFile::ReadOnly | QFile::Text);
	QString fragString;
	QTextStream fragStream(&fragFile);
	fragString.append(fragStream.readAll());
	std::string fragSTLString = fragString.toStdString();

	const GLchar* fragSource = fragSTLString.c_str();

	GLuint fragShader = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragShader, 1, &fragSource, NULL);
	glCompileShader(fragShader);
	{
		GLint compiled;
		glGetShaderiv(fragShader, GL_COMPILE_STATUS, &compiled);
		if (!compiled) {
			GLsizei len;
			glGetShaderiv(fragShader, GL_INFO_LOG_LENGTH, &len);

			GLchar* log = new GLchar[len + 1];
			glGetShaderInfoLog(fragShader, len, &len, log);
			std::cerr << "Shader compilation failed: " << log << std::endl;
			delete[] log;
		}
	}
	glAttachShader(program, fragShader);

	glLinkProgram(program);

	return program;
}

void GLWidget::mousePressEvent(QMouseEvent *event) {

	activeMenuItem = menuPanel.mouseClick(event->x(), event->y());

	if (activeMenuItem == -1) {
		vec2 pt(event->x(), event->y());

		start = pointOnVirtualTrackball(pt);
	}
	else {

		int num_buttons = menuPanel.numButtons;
		menuPanel.setMenuItemDown(activeMenuItem);

		if (activeMenuItem == 0) {
			resetView();
		}
		else if (activeMenuItem > 0) {
			int mouseX = event->x();
			int activeSlider = activeMenuItem - num_buttons;

			if (mouseX > menuPanel.getSliderStart(activeSlider).x && mouseX < menuPanel.getSliderStart(activeSlider).x + menuPanel.getSliderWidth(activeSlider)) {
				menuPanel.setSliderPos(activeSlider, mouseX);
				valueUpdate(activeSlider, menuPanel.getSliderValue(activeSlider));
			}
			else if (mouseX < menuPanel.getSliderStart(activeSlider).x) {
				menuPanel.setSliderPos(activeSlider, menuPanel.getSliderStart(activeSlider).x);
				valueUpdate(activeSlider, menuPanel.getSliderValue(activeSlider));
			}
			else if (mouseX > menuPanel.getSliderStart(activeSlider).x + menuPanel.getSliderWidth(activeSlider)) {
				menuPanel.setSliderPos(activeSlider, menuPanel.getSliderStart(activeSlider).x + menuPanel.getSliderWidth(activeSlider));
				valueUpdate(activeSlider, menuPanel.getSliderValue(activeSlider));
			}
		}

		update();
	}


}

void GLWidget::mouseMoveEvent(QMouseEvent *event) {

	int num_buttons = menuPanel.numButtons;

	if (activeMenuItem == -1) {
		vec2 pt(event->x(), event->y());

		next = pointOnVirtualTrackball(pt);

		vec3 v1 = normalize(start);
		vec3 v2 = normalize(next);

		vec3 N = cross(v1, v2);


		if (length(N) >= 0.0001) {

			float angle = acos(dot(v1, v2))*(180 / M_PI);
			mat4 rot = rotate(mat4(1.0f), angle, N);
			terrainModelMatrix = rot * terrainModelMatrix;

			glUseProgram(terrainProg);
			glUniformMatrix4fv(terrainModelMatrixLoc, 1, false, value_ptr(terrainModelMatrix));

			update();
		}

		start = next;
	}
	else if (activeMenuItem > 0) {
		int mouseX = event->x();
		int activeSlider = activeMenuItem - num_buttons;

		if (mouseX > menuPanel.getSliderStart(activeSlider).x && mouseX < menuPanel.getSliderStart(activeSlider).x + menuPanel.getSliderWidth(activeSlider)) {
			menuPanel.setSliderPos(activeSlider, mouseX);
			valueUpdate(activeSlider, menuPanel.getSliderValue(activeSlider));
		}
		else if (mouseX < menuPanel.getSliderStart(activeSlider).x) {
			menuPanel.setSliderPos(activeSlider, menuPanel.getSliderStart(activeSlider).x);
			valueUpdate(activeSlider, menuPanel.getSliderValue(activeSlider));
		}
		else if (mouseX > menuPanel.getSliderStart(activeSlider).x + menuPanel.getSliderWidth(activeSlider)) {
			menuPanel.setSliderPos(activeSlider, menuPanel.getSliderStart(activeSlider).x + menuPanel.getSliderWidth(activeSlider));
			valueUpdate(activeSlider, menuPanel.getSliderValue(activeSlider));
		}

		update();
	}
}

void GLWidget::mouseReleaseEvent(QMouseEvent *event) {
	if (activeMenuItem != -1) {
		menuPanel.setMenuItemUp(activeMenuItem);

		update();
	}
}

vec3 GLWidget::pointOnVirtualTrackball(const vec2 &pt) {
	float r = 5000;
	float r2 = r * r;

	float xP = (pt.x - (width / 2));
	float yP = (pt.y - (height / 2));

	float x2 = pow(xP, 2);
	float y2 = pow(yP, 2);

	float z;

	if (x2 + y2 <= r2 / 2) {
		z = sqrt(r2 - (x2 + y2));
	}
	else {
		z = (r2 / 2) / (sqrt(x2 + y2));
	}

	vec3 point = vec3(xP, yP, z);

	return point;
}

void GLWidget::resetView() {
	terrainModelMatrix = mat4(1.0f);

	glUseProgram(terrainProg);
	glUniformMatrix4fv(terrainModelMatrixLoc, 1, false, value_ptr(terrainModelMatrix));
}

void GLWidget::valueUpdate(int index, float value) {
	if (index == 0) {
		platforms.updateHeight(value);
	}
	else if (index == 1) {
		platforms.updateSpacing(value);
	}
	else if (index == 2) {
		platforms.updateSize(value);
	}
	else if (index == 3) {
		numShown = round(10 * value) + 1;
	}
}