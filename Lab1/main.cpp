#include <iostream>
#include <QImage>
#include <QColor>

using namespace std;

int main(int argc, char** argv) {

	float x1, x2, x3, y1, y2, y3;
	float r1, r2, r3, g1, g2, g3, b1, b2, b3;
	char colon, comma;

	cout << "Input 3 X,Y RGB sets as X,Y:R,G,B with the RGB values ranging from 0 to 1" << endl << "Set 1: ";
	cin >> x1 >> comma >> y1 >> colon >> r1 >> comma >> g1 >> comma >> b1;

	cout << "Set 2: ";
	cin >> x2 >> comma >> y2 >> colon >> r2 >> comma >> g2 >> comma >> b2;

	cout << "Set 3: ";
	cin >> x3 >> comma >> y3 >> colon >> r3 >> comma >> g3 >> comma >> b3;
	
	QImage image(640, 480, QImage::Format_RGB32);

	int Color1R = (int)r1 * 255;
	int Color1G = (int)g1 * 255;
	int Color1B = (int)b1 * 255;

	int Color2R = (int)r2 * 255;
	int Color2G = (int)g2 * 255;
	int Color2B = (int)b2 * 255;

	int Color3R = (int)r3 * 255;
	int Color3G = (int)g3 * 255;
	int Color3B = (int)b3 * 255;

	int x, y;
	for (x = 0; x < 641; x++) {
		for (y = 0; y < 480; y++) {
			float bary1, bary2, bary3;

			bary1 = ((y2 - y3)*(x - x3) + (x3 - x2)*(y - y3)) / ((y2 - y3)*(x1 - x3) + (x3 - x2)*(y1 - y3));
			bary2 = ((y3 - y1)*(x - x3) + (x1 - x3)*(y - y3)) / ((y2 - y3)*(x1 - x3) + (x3 - x2)*(y1 - y3));
			bary3 = 1 - bary1 - bary2;

			if (bary1 >= 0 && bary1 <= 1) {
				if (bary2 >= 0 && bary2 <= 1) {
					if (bary3 >= 0 && bary3 <= 1) {
						int FinalR = (int)(Color1R*bary1 + Color2R*bary2 + Color3R*bary3);
						int FinalG = (int)(Color1G*bary1 + Color2G*bary2 + Color3G*bary3);
						int FinalB = (int)(Color1B*bary1 + Color2B*bary2 + Color3B*bary3);

						image.setPixel(x, y, qRgb(FinalR, FinalG, FinalB));
					}
				}
			}
		}
	}

	if (image.save("triangle.jpg", 0, 100)) {
		cout << "Output triangle.jpg" << endl;
	}
	else {
		cout << "Unable to save triangle.jpg" << endl;
	}

	return(0);
}