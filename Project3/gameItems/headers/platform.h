#ifndef __PLATFORM__INCLUDE__
#define __PLATFORM__INCLUDE__

#include <glm/glm.hpp>
#include <vector>

#include "../../physics/headers/bounding_box.h"

using glm::vec3;
using glm::vec2;
using std::vector;

class Platform {
public:
	Platform(float startX, float startY, float startZ, float startMultiplier, float width, float length);
	~Platform();
	
	vector<vec3> getPositions();
	vector<vec3> getColors();
	vector<vec3> getNormals();
	vector<unsigned int> getIndices();

	void updateHeightMultiplier(float newHeightMultiplier);
	void updateSize(float newWidth, float newLength);
	void updatePosition(float newX, float newZ);
	void updateHeight(float newY);

	float getInitHeight();
	vec2 getInitCenter();
	vec3 getCenter();

	BoundingBox bBox;

protected:
	void locationUpdate();

private:
	float heightMultiplier;
	float length;
	float width;

	float xCenter;
	float xInit;

	float zCenter;
	float zInit;

	float yCenter;
	float yBegin;
	float yInit;

	vector<vec3> positions;
	vector<vec3> colors;
	vector<vec3> normals;
	vector<unsigned int> indices;
};

#endif