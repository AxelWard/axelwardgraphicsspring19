#ifndef __WINDIAMOND__INCLUDE__
#define __WINDIAMOND__INCLUDE__

#include <vector>
#include <glm/glm.hpp>

#include "../../physics/headers/bounding_box.h"

using glm::vec3;
using glm::mat4;

using namespace std;

class WinDiamond {
public:
	WinDiamond();
	~WinDiamond();

	void getPositions(vector<vec3>& pts);
	void getColors(vector<vec3>& clrs);
	void getNormals(vector<vec3>& nrmls);
	void getIndices(vector<unsigned int>& ind, int start);

	void updatePos(float timeDiff);
	void updateCenter(vec3 newCenter);

	BoundingBox bBox;

private:
	vec3 center;
	float angle;
	float height;
	mat4 rotation;
	mat4 translation;

	vector<vec3> positions;
	vector<vec3> pos;
	vector<vec3> colors;
	vector<vec3> normals;
	vector<vec3> initNormals;
	vector<unsigned int> indices;
};

#endif