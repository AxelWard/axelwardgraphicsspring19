#ifndef __PLAYER__INCLUDE__
#define __PLAYER__INCLUDE__

#include <glm/glm.hpp>

#include "../../physics/headers/bounding_box.h"

using glm::vec2;
using glm::vec3;
using glm::mat4;

class Player {
public:
	Player();
	~Player();

	void setPos(vec3 newPos, mat4& viewMat);

	void isGrounded();
	void startView(vec2 startPt);
	void updateView(vec2 newPt, mat4& viewMat);
	void updatePos(mat4& viewMat);
	void updateGroundLevel(float newLevel);
	void addVelocityMultiplier(vec3 newMultiplier);
	void resetVelocityMultiplier();

	vec3 getPos();

	BoundingBox bBox;

	bool forward;
	bool backward;
	bool right;
	bool left;
	bool space;
	bool shift;
	bool grounded;

	float sensitivity;

protected:



private:
	vec3 pos;
	vec3 vel;
	mat4 viewMatrix;
	mat4 pitchMatrix;
	mat4 yawMatrix;
	mat4 camTrans;
	vec2 lastPt;

	float pitch;
	float yaw;

	float groundLevel;

	vec3 velocityMultiplier;
};

#endif