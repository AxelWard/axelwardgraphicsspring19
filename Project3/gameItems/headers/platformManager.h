#ifndef __PLATFORMMANAGER__INCLUDE__
#define __PLATFORMMANAGER__INCLUDE__

#include <vector>
#include <glm/glm.hpp>

#include "../headers/platform.h"

using glm::vec3;
using std::vector;

class PlatformManager {
public:
	PlatformManager();
	~PlatformManager();

	void getPositionData(vector<vec3>& positions);
	void getColorData(vector<vec3>& colors);
	void getNormalData(vector<vec3>& normals);
	void getIndiceData(vector<unsigned int>& indices);
	vec3 getPlatformLocation(int index);

	void updateHeight(float newValue);
	void updateSize(float newWidth, float newHeight);

	void createLevel(float heightMultiplier, float spacingMultiplier, int length, int width);
	void addPlatform(float x, float y, float z, float heightMultiplier, float width, float length);
	void clearPlatforms();

	vector<Platform> platforms;

private:

	float spacingMultiplier;
};

#endif