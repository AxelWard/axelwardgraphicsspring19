#ifndef __WALL__INCLUDE__
#define __WALL__INCLUDE__

#include <glm/glm.hpp>
#include <vector>

#include "../../physics/headers/bounding_box.h"

using glm::vec3;
using glm::vec2;
using std::vector;

class Wall {
public:
	Wall(vec3 p, vec3 s, vec3 c);
	~Wall();

	void getPositions(vector<vec3>& pts);
	void getNormals(vector<vec3>& nrm);
	void getColors(vector<vec3>& clr);
	void getIndices(vector<unsigned int>& ind, int start);

	BoundingBox bBox;

private:
	vec3 position;
	vec3 size;
	vec3 color;

	vector<vec3> positions;
	vector<vec3> colors;
	vector<vec3> normals;
	vector<unsigned int> indices;

};

#endif