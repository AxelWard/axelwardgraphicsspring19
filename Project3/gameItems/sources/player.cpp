#include "../headers/player.h"

#include <iostream>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/glm.hpp>

using glm::rotate;
using glm::translate;

using namespace std;

Player::Player() {
	grounded = false;
	forward = false;
	backward = false;
	right = false;
	left = false;
	space = false;

	pos = vec3(0.0f, 5.0f, 0.0f);
	vel = vec3(0.0f);

	viewMatrix = mat4(0.0f);
	camTrans = mat4(0.0f);
	pitchMatrix = mat4(1.0f);
	yawMatrix = mat4(1.0f);

	bBox = BoundingBox();
	bBox.tfl = vec3(.5, 1, .5);
	bBox.bbr = vec3(-.5, 0, -.5);
	sensitivity = -.5;
}

Player::~Player() {

}

void Player::isGrounded() {
	grounded = true;
}

void Player::startView(vec2 startPt) {
	lastPt = startPt;
}

void Player::updateView(vec2 newPt, mat4& viewMat) {
	vec2 d = newPt - lastPt;

	const int clamp = 75;

	pitch = pitch + (d.y * sensitivity);
	yaw = yaw + (d.x * sensitivity);

	if (pitch < -clamp) {
		pitch = -clamp;
	}
	else if (pitch > clamp) {
		pitch = clamp;
	}

	pitchMatrix = rotate(mat4(1.0f), (float)((pitch * M_PI) / 180), vec3(1, 0, 0));
	yawMatrix = rotate(mat4(1.0f), (float)((yaw * M_PI) / 180), vec3(0, 1, 0));
	camTrans = translate(mat4(1.0f), pos);

	viewMat = camTrans * yawMatrix * pitchMatrix;
	viewMatrix = viewMat;

	lastPt = newPt;
}

void Player::updatePos(mat4& viewMat) {
	const float time = .016;
	const float speed = 4.0f;
	const float jumpHeight = 1.0f;
	const float gravity = 1.7f;

	float sprint = 1.0f;

	if (shift) {
		sprint = 1.7f;
	}

	if (space) {
		if (bBox.bbr.y > groundLevel) {
			vel.y -= 10.0f * time * gravity;
			grounded = false;
		}
		else if (grounded) {
			vel.y = 10.0f * jumpHeight;
			grounded = false;
		}
		else {
			vel.y = 0.0f;
			pos.y = groundLevel + .5;
			bBox.tfl.y = groundLevel + 1;
			bBox.bbr.y = groundLevel;
			isGrounded();
		}
	}
	else {
		if (bBox.bbr.y > groundLevel) {
			vel.y -= 10.0f * time * gravity;
			grounded = false;
		}
		else if (bBox.bbr.y <= groundLevel) {
			vel.y = 0.0f;
			pos.y = groundLevel + .5;
			bBox.tfl.y = groundLevel + 1;
			bBox.bbr.y = groundLevel;
			isGrounded();
		}
	}

	vec3 camForward = vec3(0, 0, 0);
	camForward.x = -sin((yaw * M_PI) / 180);
	camForward.y = 0.0f;
	camForward.z = -cos((yaw * M_PI) / 180);

	vec3 camRight = vec3(0, 0, 0);
	camRight.x = -cos((yaw * M_PI) / 180);
	camRight.y = 0;
	camRight.z = sin((yaw * M_PI) / 180);

	vec3 camFront = vec3(0, vel.y, 0);
	if (forward) {
		camFront += camForward * speed * sprint;
	}
	if (backward) {
		camFront -= camForward * speed * sprint;
	}

	vec3 camLat = vec3(0, 0, 0);
	if (left) {
		camLat += camRight * speed * sprint;
	}
	if (right) {
		camLat -= camRight * speed * sprint;
	}

	vel = (camFront + camLat);

	if (velocityMultiplier.y < 0) {
		if (vel.y < 0) {
			vel.y = 0;
		}
	}
	else if (velocityMultiplier.y > 0) {
		if (vel.y > 0) {
			vel.y = 0;
		}
	}

	if (velocityMultiplier.x < 0) {
		if (vel.x < 0) {
			vel.x = 0;
		}
	}
	else if (velocityMultiplier.x > 0) {
		if (vel.x > 0) {
			vel.x = 0;
		}
	}

	if (velocityMultiplier.z < 0) {
		if (vel.z < 0) {
			vel.z = 0;
		}
	}
	else if (velocityMultiplier.z > 0) {
		if (vel.z > 0) {
			vel.z = 0;
		}
	}

	if (vel.y < -10) {
		vel.y = -10;
	}

	pos = pos + (vel * time);
	camTrans = translate(mat4(1.0f), pos);

	bBox.tfl = bBox.tfl + (vel * time);
	bBox.bbr = bBox.bbr + (vel * time);

	viewMat = camTrans * yawMatrix * pitchMatrix;
	viewMatrix = viewMat;
}

void Player::updateGroundLevel(float newLevel) {
	groundLevel = newLevel;
}

void Player::resetVelocityMultiplier() {
	velocityMultiplier = vec3(0.0f);
}

void Player::addVelocityMultiplier(vec3 newMultiplier) {
	velocityMultiplier += newMultiplier;
}

glm::vec3 Player::getPos() {
	return pos;
}

void Player::setPos(vec3 newPos, mat4& viewMat) {
	pos = newPos;
	bBox.tfl = vec3(newPos.x + .5, newPos.y + 1, newPos.z + .5);
	bBox.bbr = vec3(newPos.x - .5, newPos.y + 0, newPos.z -.5);
	
	vel = vec3(0.0f);

	camTrans = translate(mat4(1.0f), pos);
	viewMat = camTrans * yawMatrix * pitchMatrix;
	viewMatrix = viewMat;
}