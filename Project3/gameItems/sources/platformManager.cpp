#include "../headers/platformManager.h"

#include <iostream>
#include <random>

using glm::vec3;

using namespace std;

PlatformManager::PlatformManager() {
}

PlatformManager::~PlatformManager() {
}

void PlatformManager::addPlatform(float x, float y, float z, float heightMultiplier, float width, float length) {
	Platform temp = Platform(x, y, z, heightMultiplier, width, length);
	platforms.push_back(temp);
}

void PlatformManager::getPositionData(vector<vec3> &positions) {
	positions.clear();
	for (unsigned int i = 0; i < platforms.size(); i++) {
		vector<vec3> temp = platforms[i].getPositions();

		for (int j = 0; j < 24; j++) {
			positions.push_back(temp[j]);
		}
	}
}

void PlatformManager::getColorData(vector<vec3>& colors) {
	colors.clear();
	for (unsigned int i = 0; i < platforms.size(); i++) {
		vector<vec3> temp = platforms[i].getColors();

		for (int j = 0; j < 24; j++) {
			colors.push_back(temp[j]);
		}
	}
}

void PlatformManager::getNormalData(vector<vec3>& normals) {
	normals.clear();
	for (unsigned int i = 0; i < platforms.size(); i++) {
		vector<vec3> temp = platforms[i].getNormals();

		for (int j = 0; j < 24; j++) {
			normals.push_back(temp[j]);
		}
	}
}

void PlatformManager::getIndiceData(vector<unsigned int>& indices) {
	indices.clear();
	for (unsigned int i = 0; i < platforms.size(); i++) {
		vector<unsigned int> temp = platforms[i].getIndices();

		for (int j = 0; j < 36; j++) {
			indices.push_back(temp[j] + (i*24));
		}
	}
}

vec3 PlatformManager::getPlatformLocation(int index) {
	return(platforms[index].getCenter());
}


void PlatformManager::updateHeight(float newValue) {
	for (unsigned int i = 0; i < platforms.size(); i++) {
		platforms[i].updateHeightMultiplier((newValue));
	}
}

void PlatformManager::updateSize(float newWidth, float newLength) {
	for (unsigned int i = 0; i < platforms.size(); i++) {
		platforms[i].updateSize(newWidth, newLength);
	}
}

void PlatformManager::clearPlatforms() {
	platforms.clear();
}

void PlatformManager::createLevel(float heightMultiplier, float spacingMultiplier, int length, int width) {
	addPlatform(0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f);

	for (int i = 0; i < width; i++) {
		for (int j = 0; j < length; j++) {
			float randHeight = ((double)rand() / (RAND_MAX + 1));
			if (i == 0 && j == 0) {
				addPlatform(-(j + 1) * spacingMultiplier, randHeight, -(i + 1) * spacingMultiplier, heightMultiplier*.5, 1.0f, 1.0f);
			}
			else {
				addPlatform(-(j + 1) * spacingMultiplier, randHeight, -(i + 1) * spacingMultiplier, heightMultiplier, 1.0f, 1.0f);
			}
		}
	}
}