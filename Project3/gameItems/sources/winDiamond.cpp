#include "../headers/winDiamond.h"

#include <iostream>
#include <math.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#define PI 3.14159265

using glm::normalize;
using glm::cross;
using glm::rotate;
using glm::inverse;
using glm::transpose;
using glm::translate;
using glm::vec4;

using namespace std;

WinDiamond::WinDiamond() {
	center = vec3(0.0f);

	bBox.tfl = vec3(center.x + 0.25f, center.y + 0.5f, center.z + 0.25f);
	bBox.bbr = vec3(center.x - 0.25f, center.y - 0.5f, center.z - 0.25f);

	// top half
	positions.push_back(vec3(0, center.y + 0.5f, 0));
	positions.push_back(vec3(0.25f, center.y + 0, 0.25f));
	positions.push_back(vec3(-0.25f, center.y + 0, 0.25f));

	positions.push_back(vec3(0, center.y + 0.5f, 0));
	positions.push_back(vec3(0.25f, center.y, -0.25f));
	positions.push_back(vec3(0.25f, center.y, 0.25f));

	positions.push_back(vec3(0, center.y + 0.5f, 0));
	positions.push_back(vec3(-0.25f, center.y + 0, -0.25f));
	positions.push_back(vec3(0.25f, center.y + 0, -0.25f));

	positions.push_back(vec3(0, center.y + 0.5f, 0));
	positions.push_back(vec3(-0.25f, center.y + 0, 0.25f));
	positions.push_back(vec3(-0.25f, center.y + 0, -0.25f));

	//bottom half
	positions.push_back(vec3(0, center.y - 0.5f, 0));
	positions.push_back(vec3(-0.25f, center.y + 0, 0.25f));
	positions.push_back(vec3(0.25f, center.y + 0, 0.25f));

	positions.push_back(vec3(0, center.y - 0.5f, 0));
	positions.push_back(vec3(0.25f, center.y, 0.25f));
	positions.push_back(vec3(0.25f, center.y, -0.25f));

	positions.push_back(vec3(0, center.y - 0.5f, 0));
	positions.push_back(vec3(0.25f, center.y + 0, -0.25f));
	positions.push_back(vec3(-0.25f, center.y + 0, -0.25f));

	positions.push_back(vec3(0, center.y - 0.5f, 0));
	positions.push_back(vec3(-0.25f, center.y + 0, -0.25f));
	positions.push_back(vec3(-0.25f, center.y + 0, 0.25f));

	for (int i = 0; i < 24; i++) {
		colors.push_back(vec3(1.0f, 1.0f, 0.2f));
	}

	for (int i = 0; i < 8; i++) {
		vec3 p0 = positions[(i * 3)];
		vec3 p1 = positions[(i * 3) + 1];
		vec3 p2 = positions[(i * 3) + 2];

		vec3 n = normalize(cross(p2 - p0, p1 - p0));

		initNormals.push_back(n);
		initNormals.push_back(n);
		initNormals.push_back(n);
	}

	for (int i = 0; i < 24; i++) {
		indices.push_back(i);
	}
}

WinDiamond::~WinDiamond() {

}

void WinDiamond::getPositions(vector<vec3>& pts) {
	for (int i = 0; i < 24; i++) {
		pts.push_back(pos[i]);
	}
}

void WinDiamond::getColors(vector<vec3>& clrs) {
	for (int i = 0; i < 24; i++) {
		clrs.push_back(colors[i]);
	}
}

void WinDiamond::getNormals(vector<vec3>& nrmls) {
	for (int i = 0; i < 24; i++) {
		nrmls.push_back(normals[i]);
	}
}

void WinDiamond::getIndices(vector<unsigned int>& ind, int start) {
	for (int i = 0; i < 24; i++) {
		ind.push_back(indices[i] + start);
	}
}

void WinDiamond::updatePos(float timeDiff) {
	const float heightMultiplier = .5;
	const float spinMultiplier = .1;

	angle += timeDiff * spinMultiplier;
	height += timeDiff * heightMultiplier;

	float tempHeight = (float)sin(height);

	translation = translate(mat4(1.0f), center);
	rotation = rotate(mat4(1.0f), angle, vec3(0, 1, 0));

	mat4 modelMatrix = translation * rotation;

	for (int i = 0; i < 24; i++) {
		pos[i] = vec3(modelMatrix * vec4(positions[i], 1.0f));
		pos[i].y = positions[i].y + (tempHeight * .05);

		normals[i] = vec3(rotation * vec4(initNormals[i], 0.0f));
	}
}

void WinDiamond::updateCenter(vec3 newCenter) {

	center = newCenter;
	height = 0;
	angle = 0;

	bBox.tfl = vec3(center.x + 0.25f, center.y + 0.5f, center.z + 0.25f);
	bBox.bbr = vec3(center.x - 0.25f, center.y - 0.5f, center.z - 0.25f);

	positions.clear();
	initNormals.clear();

	// top half
	positions.push_back(vec3(0, center.y + 0.5f, 0));
	positions.push_back(vec3(0.25f, center.y + 0, 0.25f));
	positions.push_back(vec3(-0.25f, center.y + 0, 0.25f));

	positions.push_back(vec3(0, center.y + 0.5f, 0));
	positions.push_back(vec3(0.25f, center.y, -0.25f));
	positions.push_back(vec3(0.25f, center.y, 0.25f));

	positions.push_back(vec3(0, center.y + 0.5f, 0));
	positions.push_back(vec3(-0.25f, center.y + 0, -0.25f));
	positions.push_back(vec3(0.25f, center.y + 0, -0.25f));

	positions.push_back(vec3(0, center.y + 0.5f, 0));
	positions.push_back(vec3(-0.25f, center.y + 0, 0.25f));
	positions.push_back(vec3(-0.25f, center.y + 0, -0.25f));

	//bottom half
	positions.push_back(vec3(0, center.y - 0.5f, 0));
	positions.push_back(vec3(-0.25f, center.y + 0, 0.25f));
	positions.push_back(vec3(0.25f, center.y + 0, 0.25f));

	positions.push_back(vec3(0, center.y - 0.5f, 0));
	positions.push_back(vec3(0.25f, center.y, 0.25f));
	positions.push_back(vec3(0.25f, center.y, -0.25f));

	positions.push_back(vec3(0, center.y - 0.5f, 0));
	positions.push_back(vec3(0.25f, center.y + 0, -0.25f));
	positions.push_back(vec3(-0.25f, center.y + 0, -0.25f));

	positions.push_back(vec3(0, center.y - 0.5f, 0));
	positions.push_back(vec3(-0.25f, center.y + 0, -0.25f));
	positions.push_back(vec3(-0.25f, center.y + 0, 0.25f));

	for (int i = 0; i < 8; i++) {
		vec3 p0 = positions[(i * 3)];
		vec3 p1 = positions[(i * 3) + 1];
		vec3 p2 = positions[(i * 3) + 2];

		vec3 n = normalize(cross(p2 - p0, p1 - p0));

		initNormals.push_back(n);
		initNormals.push_back(n);
		initNormals.push_back(n);
	}

	pos = positions;
	normals = initNormals;
}