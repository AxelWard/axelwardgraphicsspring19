#include "../headers/platform.h"

#include <iostream>
#include <glm/glm.hpp>

#include "../../physics/headers/bounding_box.h"

using glm::vec3;

using namespace std;

Platform::Platform(float startX, float startY, float startZ, float startMultiplier, float startWidth, float startLength) {

	yInit = startY * 5;
	xInit = startX;
	zInit = startZ;

	bBox = BoundingBox();

	xCenter = startX;
	yBegin = yInit;
	zCenter = startZ;
	heightMultiplier = startMultiplier;
	width = startWidth;
	length = startLength;

	yCenter = yInit * heightMultiplier;

	bBox.tfl = vec3(width + xCenter, .25 + yCenter, length + zCenter);
	bBox.bbr = vec3(-width + xCenter, -.25 + yCenter, -length + zCenter);

	//positions
		// top
	positions.push_back(vec3(width + xCenter, .25 + yCenter, length + zCenter)); // 0
	positions.push_back(vec3(width + xCenter, .25 + yCenter, -length + zCenter)); // 1
	positions.push_back(vec3(-width + xCenter, .25 + yCenter, -length + zCenter)); // 2
	positions.push_back(vec3(-width + xCenter, .25 + yCenter, length + zCenter)); // 3

		// bottom
	positions.push_back(vec3(width + xCenter, -.25 + yCenter, length + zCenter)); // 4
	positions.push_back(vec3(-width + xCenter, -.25 + yCenter, length + zCenter)); // 5
	positions.push_back(vec3(-width + xCenter, -.25 + yCenter, -length + zCenter)); // 6
	positions.push_back(vec3(width + xCenter, -.25 + yCenter, -length + zCenter)); // 7

		// front
	positions.push_back(vec3(width + xCenter, .25 + yCenter, length + zCenter)); // 8
	positions.push_back(vec3(-width + xCenter, .25 + yCenter, length + zCenter)); // 9
	positions.push_back(vec3(-width + xCenter, -.25 + yCenter, length + zCenter)); // 10
	positions.push_back(vec3(width + xCenter, -.25 + yCenter, length + zCenter)); // 11

		// back
	positions.push_back(vec3(-width + xCenter, -.25 + yCenter, -length + zCenter)); // 12
	positions.push_back(vec3(-width + xCenter, .25 + yCenter, -length + zCenter)); // 13
	positions.push_back(vec3(width + xCenter, .25 + yCenter, -length + zCenter)); // 14
	positions.push_back(vec3(width + xCenter, -.25 + yCenter, -length + zCenter)); // 15

		// right
	positions.push_back(vec3(width + xCenter, -.25 + yCenter, length + zCenter)); // 16
	positions.push_back(vec3(width + xCenter, -.25 + yCenter, -length + zCenter)); // 17
	positions.push_back(vec3(width + xCenter, .25 + yCenter, -length + zCenter)); // 18
	positions.push_back(vec3(width + xCenter, .25 + yCenter, length + zCenter)); // 19

		// left
	positions.push_back(vec3(-width + xCenter, -.25 + yCenter, length + zCenter)); // 20
	positions.push_back(vec3(-width + xCenter, .25 + yCenter, length + zCenter)); // 21
	positions.push_back(vec3(-width + xCenter, .25 + yCenter, -length + zCenter)); // 22
	positions.push_back(vec3(-width + xCenter, -.25 + yCenter, -length + zCenter));  // 23

	//colors
		// top
	colors.push_back(vec3(.75f, .75f, .75f));
	colors.push_back(vec3(.75f, .75f, .75f));
	colors.push_back(vec3(.75f, .75f, .75f));
	colors.push_back(vec3(.75f, .75f, .75f));

		// bottom
	colors.push_back(vec3(.25f, .25f, .25f));
	colors.push_back(vec3(.25f, .25f, .25f));
	colors.push_back(vec3(.25f, .25f, .25f));
	colors.push_back(vec3(.25f, .25f, .25f));

		// front
	colors.push_back(vec3(.5f, .5f, .5f));
	colors.push_back(vec3(.5f, .5f, .5f));
	colors.push_back(vec3(.5f, .5f, .5f));
	colors.push_back(vec3(.5f, .5f, .5f));

		// back
	colors.push_back(vec3(.5f, .5f, .5f));
	colors.push_back(vec3(.5f, .5f, .5f));
	colors.push_back(vec3(.5f, .5f, .5f));
	colors.push_back(vec3(.5f, .5f, .5f));

		// right
	colors.push_back(vec3(.5f, .5f, .5f));
	colors.push_back(vec3(.5f, .5f, .5f));
	colors.push_back(vec3(.5f, .5f, .5f));
	colors.push_back(vec3(.5f, .5f, .5f));
								
		// left
	colors.push_back(vec3(.5f, .5f, .5f));
	colors.push_back(vec3(.5f, .5f, .5f));
	colors.push_back(vec3(.5f, .5f, .5f));
	colors.push_back(vec3(.5f, .5f, .5f));

	//normals
		// top
	normals.push_back(vec3(0, 1, 0));  // 0
	normals.push_back(vec3(0, 1, 0));  // 1
	normals.push_back(vec3(0, 1, 0));  // 2
	normals.push_back(vec3(0, 1, 0));  // 3

		// bottom
	normals.push_back(vec3(0, -1, 0)); // 4
	normals.push_back(vec3(0, -1, 0)); // 5
	normals.push_back(vec3(0, -1, 0)); // 6
	normals.push_back(vec3(0, -1, 0)); // 7

		// front
	normals.push_back(vec3(0, 0, 1));  // 8
	normals.push_back(vec3(0, 0, 1));  // 9
	normals.push_back(vec3(0, 0, 1));  // 10
	normals.push_back(vec3(0, 0, 1));  // 11

		// back
	normals.push_back(vec3(0, 0, -1)); // 12
	normals.push_back(vec3(0, 0, -1)); // 13
	normals.push_back(vec3(0, 0, -1)); // 14
	normals.push_back(vec3(0, 0, -1)); // 15

		// right
	normals.push_back(vec3(1, 0, 0));  // 16
	normals.push_back(vec3(1, 0, 0));  // 17
	normals.push_back(vec3(1, 0, 0));  // 18
	normals.push_back(vec3(1, 0, 0));  // 19

		// left
	normals.push_back(vec3(-1, 0, 0)); // 20
	normals.push_back(vec3(-1, 0, 0)); // 21
	normals.push_back(vec3(-1, 0, 0)); // 22
	normals.push_back(vec3(-1, 0, 0)); // 23
	
	//indices
		//top
	indices.push_back(0);
	indices.push_back(1);
	indices.push_back(2);
	indices.push_back(0);
	indices.push_back(2);
	indices.push_back(3);

		//bottom
	indices.push_back(4);
	indices.push_back(5);
	indices.push_back(6);
	indices.push_back(4);
	indices.push_back(6);
	indices.push_back(7);

		//front
	indices.push_back(8);
	indices.push_back(9);
	indices.push_back(10);
	indices.push_back(8);
	indices.push_back(10);
	indices.push_back(11);

		//back
	indices.push_back(12);
	indices.push_back(13);
	indices.push_back(14);
	indices.push_back(12);
	indices.push_back(14);
	indices.push_back(15);

		//right
	indices.push_back(16);
	indices.push_back(17);
	indices.push_back(18);
	indices.push_back(16);
	indices.push_back(18);
	indices.push_back(19);

		//left
	indices.push_back(20);
	indices.push_back(21);
	indices.push_back(22);
	indices.push_back(20);
	indices.push_back(22);
	indices.push_back(23);
}

Platform::~Platform() {
}


//Retrieval
vector<vec3> Platform::getPositions() {
	return(positions);
}

vector <vec3> Platform::getColors() {
	return(colors);
}

vector <vec3> Platform::getNormals() {
	return(normals);
}

vector<unsigned int> Platform::getIndices() {
	return(indices);
}


//Update
void Platform::updateHeightMultiplier(float newHeightMultiplier) {
	heightMultiplier = newHeightMultiplier;
	locationUpdate();
}

void Platform::updateSize(float newWidth, float newLength) {
	width = newWidth;
	length = newLength;
	locationUpdate();
}

void Platform::updatePosition(float newX, float newZ) {
	xCenter = newX;
	zCenter = newZ;
	locationUpdate();
}

void Platform::updateHeight(float newY) {
	yBegin = newY;
	locationUpdate();
}

void Platform::locationUpdate() {
	positions.clear();

	yCenter = yInit * heightMultiplier;

	bBox.tfl = vec3(width + xCenter, .25 + yCenter, length + zCenter);
	bBox.bbr = vec3(-width + xCenter, -.25 + yCenter, -length + zCenter);

	//positions
		// top
	positions.push_back(vec3(width + xCenter, .25 + yCenter, length + zCenter)); // 0
	positions.push_back(vec3(width + xCenter, .25 + yCenter, -length + zCenter)); // 1
	positions.push_back(vec3(-width + xCenter, .25 + yCenter, -length + zCenter)); // 2
	positions.push_back(vec3(-width + xCenter, .25 + yCenter, length + zCenter)); // 3

		// bottom
	positions.push_back(vec3(width + xCenter, -.25 + yCenter, length + zCenter));   // 4
	positions.push_back(vec3(-width + xCenter, -.25 + yCenter, length + zCenter));  // 5
	positions.push_back(vec3(-width + xCenter, -.25 + yCenter, -length + zCenter)); // 6
	positions.push_back(vec3(width + xCenter, -.25 + yCenter, -length + zCenter));  // 7

		// front
	positions.push_back(vec3(width + xCenter, .25 + yCenter, length + zCenter)); // 8
	positions.push_back(vec3(-width + xCenter, .25 + yCenter, length + zCenter)); // 9
	positions.push_back(vec3(-width + xCenter, -.25 + yCenter, length + zCenter)); // 10
	positions.push_back(vec3(width + xCenter, -.25 + yCenter, length + zCenter)); // 11

		// back
	positions.push_back(vec3(-width + xCenter, -.25 + yCenter, -length + zCenter)); // 12
	positions.push_back(vec3(-width + xCenter, .25 + yCenter, -length + zCenter)); // 13
	positions.push_back(vec3(width + xCenter, .25 + yCenter, -length + zCenter)); // 14
	positions.push_back(vec3(width + xCenter, -.25 + yCenter, -length + zCenter)); // 15

		// right
	positions.push_back(vec3(width + xCenter, -.25 + yCenter, length + zCenter)); // 16
	positions.push_back(vec3(width + xCenter, -.25 + yCenter, -length + zCenter)); // 17
	positions.push_back(vec3(width + xCenter, .25 + yCenter, -length + zCenter)); // 18
	positions.push_back(vec3(width + xCenter, .25 + yCenter, length + zCenter)); // 19

		// left
	positions.push_back(vec3(-width + xCenter, -.25 + yCenter, length + zCenter)); // 20
	positions.push_back(vec3(-width + xCenter, .25 + yCenter, length + zCenter)); // 21
	positions.push_back(vec3(-width + xCenter, .25 + yCenter, -length + zCenter)); // 22
	positions.push_back(vec3(-width + xCenter, -.25 + yCenter, -length + zCenter));  // 23

}

float Platform::getInitHeight() {
	return yInit;
}

vec2 Platform::getInitCenter() {
	return vec2(xInit, zInit);
}

vec3 Platform::getCenter() {
	return vec3(xCenter, yCenter, zCenter);
}