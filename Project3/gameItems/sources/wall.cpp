#include "../headers/wall.h"

#include <glm/glm.hpp>

using glm::vec3;

using namespace std;

Wall::Wall(vec3 p, vec3 s, vec3 c) {
	bBox = BoundingBox();
	bBox.bbr = p;
	bBox.tfl = p + s;

		// top
	positions.push_back(p + vec3(0, s.y, 0));
	positions.push_back(p + vec3(s.x, s.y, 0));
	positions.push_back(p + vec3(0, s.y, s.z));
	positions.push_back(p + vec3(s.x, s.y, s.z));

		// bottom
	positions.push_back(p);
	positions.push_back(p + vec3(s.x, 0, 0));
	positions.push_back(p + vec3(0, 0, s.z));
	positions.push_back(p + vec3(s.x, 0, s.z));

		// front
	positions.push_back(p + vec3(0, 0, s.z));
	positions.push_back(p + vec3(0, s.y, s.z));
	positions.push_back(p + vec3(s.x, 0, s.z));
	positions.push_back(p + vec3(s.x, s.y, s.z));

		// back
	positions.push_back(p);
	positions.push_back(p + vec3(0, 0, s.z));
	positions.push_back(p + vec3(0, s.y, 0));
	positions.push_back(p + vec3(0, s.y, s.z));

		// right
	positions.push_back(p + vec3(s.x, 0, 0));
	positions.push_back(p + vec3(s.x, s.y, 0));
	positions.push_back(p + vec3(s.x, 0, s.z));
	positions.push_back(p + vec3(s.x, s.y, s.z));

		// left
	positions.push_back(p);
	positions.push_back(p + vec3(s.x, 0, 0));
	positions.push_back(p + vec3(0, s.y, 0));
	positions.push_back(p + vec3(s.x, s.y, 0));

	//indices
//top
	indices.push_back(0);
	indices.push_back(1);
	indices.push_back(2);
	indices.push_back(1);
	indices.push_back(2);
	indices.push_back(3);

	//bottom
	indices.push_back(4);
	indices.push_back(5);
	indices.push_back(6);
	indices.push_back(5);
	indices.push_back(6);
	indices.push_back(7);

	//front
	indices.push_back(8);
	indices.push_back(9);
	indices.push_back(10);
	indices.push_back(9);
	indices.push_back(10);
	indices.push_back(11);

	//back
	indices.push_back(12);
	indices.push_back(13);
	indices.push_back(14);
	indices.push_back(13);
	indices.push_back(14);
	indices.push_back(15);

	//right
	indices.push_back(16);
	indices.push_back(17);
	indices.push_back(18);
	indices.push_back(17);
	indices.push_back(18);
	indices.push_back(19);

	//left
	indices.push_back(20);
	indices.push_back(21);
	indices.push_back(22);
	indices.push_back(21);
	indices.push_back(22);
	indices.push_back(23);

	for (int i = 0; i < 24; i++) {
		colors.push_back(c);
	}

		//top
	normals.push_back(vec3(0, 1, 0));  // 0
	normals.push_back(vec3(0, 1, 0));  // 1
	normals.push_back(vec3(0, 1, 0));  // 2
	normals.push_back(vec3(0, 1, 0));  // 3

		// bottom
	normals.push_back(vec3(0, -1, 0)); // 4
	normals.push_back(vec3(0, -1, 0)); // 5
	normals.push_back(vec3(0, -1, 0)); // 6
	normals.push_back(vec3(0, -1, 0)); // 7

		// front
	normals.push_back(vec3(0, 0, 1));  // 8
	normals.push_back(vec3(0, 0, 1));  // 9
	normals.push_back(vec3(0, 0, 1));  // 10
	normals.push_back(vec3(0, 0, 1));  // 11

		// back
	normals.push_back(vec3(0, 0, -1)); // 12
	normals.push_back(vec3(0, 0, -1)); // 13
	normals.push_back(vec3(0, 0, -1)); // 14
	normals.push_back(vec3(0, 0, -1)); // 15

		// right
	normals.push_back(vec3(1, 0, 0));  // 16
	normals.push_back(vec3(1, 0, 0));  // 17
	normals.push_back(vec3(1, 0, 0));  // 18
	normals.push_back(vec3(1, 0, 0));  // 19

		// left
	normals.push_back(vec3(-1, 0, 0)); // 20
	normals.push_back(vec3(-1, 0, 0)); // 21
	normals.push_back(vec3(-1, 0, 0)); // 22
	normals.push_back(vec3(-1, 0, 0)); // 23
}

Wall::~Wall() {
}

void Wall::getPositions(vector<vec3>& pts) {
	for (int i = 0; i < 24; i++) {
		pts.push_back(positions[i]);
	}
}

void Wall::getColors(vector<vec3>& clr) {
	for (int i = 0; i < 24; i++) {
		clr.push_back(colors[i]);
	}
}

void Wall::getNormals(vector<vec3>& nrm) {
	for (int i = 0; i < 24; i++) {
		nrm.push_back(normals[i]);
	}
}

void Wall::getIndices(vector<unsigned int>& ind, int start) {
	for (int i = 0; i < 36; i++) {
		ind.push_back(indices[i] + start);
	}
}