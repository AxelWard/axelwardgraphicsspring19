HEADERS += glwidget.h
HEADERS += "./menuItems/headers/button.h" "./menuItems/headers/slider.h" "./menuItems/headers/panel.h"
HEADERS += "./gameItems/headers/platform.h" "./gameItems/headers/platformManager.h" "./gameItems/headers/player.h"
HEADERS += "./gameItems/headers/winDiamond.h" "./gameItems/headers/wall.h"
HEADERS += "./physics/headers/bounding_box.h" "./physics/headers/collisionDetector.h"

SOURCES += glwidget.cpp main.cpp
SOURCES += "./gameItems/sources/platform.cpp" "./gameItems/sources/platformManager.cpp" "./gameItems/sources/player.cpp"
SOURCES += "./gameItems/sources/winDiamond.cpp" "./gameItems/sources/wall.cpp"
SOURCES += "./menuItems/sources/button.cpp" "./menuItems/sources/slider.cpp" "./menuItems/sources/panel.cpp"
SOURCES += "./physics/sources/bounding_box.cpp" "./physics/sources/collisionDetector.cpp"

QT += opengl designer
CONFIG -= app_bundle
CONFIG += console c++11
INCLUDEPATH += "../include"

INCLUDEPATH += $$PWD

RESOURCES += shaders.qrc