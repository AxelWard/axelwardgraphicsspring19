#include "../headers/collisionDetector.h"

#include <glm/glm.hpp>

#include "../../gameItems/headers/platform.h"
#include "../../gameItems/headers/player.h"

using glm::vec3;

CollisionDetector::CollisionDetector() {

}

CollisionDetector::~CollisionDetector() {

}

bool CollisionDetector::checkBBoxes(const BoundingBox& bbox1, const BoundingBox& bbox2) {
	if ((bbox1.tfl.x >= bbox2.bbr.x && bbox1.bbr.x <= bbox2.tfl.x) &&
		(bbox1.tfl.y >= bbox2.bbr.y && bbox1.bbr.y <= bbox2.tfl.y) &&
		(bbox1.tfl.z >= bbox2.bbr.z && bbox1.bbr.z <= bbox2.tfl.z)) {
		return true;
	}

	return false;
}

void CollisionDetector::getPlayerDirection(Player& play, const BoundingBox& b) {
	const float thresh = .08;

	BoundingBox a = play.bBox;

	vec3 direction = vec3(1.0f);

	if (a.bbr.y > b.tfl.y - .15) {
		//box is below
		direction = vec3(0.0f, -1.0f, 0.0f);
		play.updateGroundLevel(b.tfl.y);
	}
	else if (a.tfl.y < b.bbr.y + thresh) {
		//box is above
		direction = vec3(0.0f, 1.0f, 0.0f);
	}
	else if (a.tfl.z < b.bbr.z + thresh) {
		//box is in front
		direction = vec3(0.0f, 0.0f, 1.0f);
	}
	else if (a.bbr.z > b.tfl.z - thresh) {
		//box is behind
		direction = vec3(0.0f, 0.0f, -1.0f);
	}
	else if (a.tfl.x < b.bbr.x + thresh) {
		//box is to the left
		direction = vec3(1.0f, 0.0f, 0.0f);
	}
	else if (a.bbr.x > b.tfl.x - thresh) {
		//box is to the right
		direction = vec3(-1.0f, 0.0f, 0.0f);
	}

	play.addVelocityMultiplier(direction);
}