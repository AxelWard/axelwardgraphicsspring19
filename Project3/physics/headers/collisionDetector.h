#ifndef __COLLISIONDETECTOR__INCLUDE__
#define __COLLISIONDETECTOR_INCLUDE__

#include "bounding_box.h"
#include "../../gameItems/headers/platform.h"
#include "../../gameItems/headers/player.h"

class CollisionDetector {
public:
	CollisionDetector();
	~CollisionDetector();

	bool checkBBoxes(const BoundingBox& bbox1, const BoundingBox& bbox2);
	void getPlayerDirection(Player& play, const BoundingBox& b);
};

#endif