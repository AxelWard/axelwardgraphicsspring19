#ifndef __BOUNDING_BOX__INCLUDE__
#define __BOUNDING_BOX__INCLUDE__

#include <glm/glm.hpp>

using glm::vec3;

class BoundingBox {
public:
	BoundingBox();
	~BoundingBox();

	vec3 tfl;
	vec3 bbr;
};

#endif