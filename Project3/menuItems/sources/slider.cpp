#include "../headers/slider.h"

#include <glm/glm.hpp>
#include <algorithm>

using glm::vec2;
using glm::vec3;
using std::max;

Slider::Slider(vec2 tl, int w, vec3 inColor, vec3 inBarColor) {
	topLeft = tl;
	width = w;

	upColor = inColor;
	downColor = vec3(max(0.0, inColor.x - .3), max(0.0, inColor.y - .3), max(0.0, inColor.z - .3));
	color = upColor;
	barColor = inBarColor;

	sliderPos = topLeft.x;
}

Slider::~Slider() {

}

bool Slider::mouseClick(int x, int y) {
	if ((x > topLeft.x - 5) && (x < topLeft.x + width + 5)) {
		if ((y > topLeft.y) && (y < topLeft.y + 25)) {
			return true;
		}
	}

	return false;
}

float Slider::sliderVal() {
	return (((float) sliderPos - topLeft.x) / width);
}

void Slider::sliderDown() {
	color = downColor;
}

void Slider::sliderUp() {
	color = upColor;
}

void Slider::updatePos(int newPos) {
	sliderPos = newPos;
}