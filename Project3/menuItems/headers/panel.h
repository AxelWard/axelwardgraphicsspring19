#ifndef __PANEL__INCLUDE__
#define __PANEL__INCLUDE__

#include <vector>
#include <glm/glm.hpp>

#include "../headers/button.h"
#include "../headers/slider.h"

using glm::vec2;

class Panel {

public:
	Panel();
	~Panel();

	void addButton(vec2 tl, int w, int h, vec3 color);
	void addSlider(vec2 tl, int w, vec3 color, vec3 barColor);
	void clearButtons();
	void clearSliders();

	int mouseClick(int x, int y);

	int numButtons;
	int numSliders;

	vec2 getButtonStart(int index);
	int getButtonWidth(int index);
	int getButtonHeight(int index);
	vec3 getButtonColor(int index);

	void setMenuItemDown(int item);
	void setMenuItemUp(int item);

	vec2 getSliderStart(int index);
	int getSliderWidth(int index);
	vec3 getSliderBarColor(int index);
	vec3 getSliderColor(int index);
	float getSliderValue(int index);
	int getSliderPos(int index);
	void setSliderPos(int index, int newPos);


private:
	std::vector<Button> buttons;
	std::vector<Slider> sliders;
};

#endif