#ifndef __SLIDER__INCLUDE__
#define __SLIDER__INCLUDE__

#include <glm/glm.hpp>

using glm::vec2;
using glm::vec3;

class Slider {
public:
	Slider(vec2 tl, int w, vec3 inColor, vec3 inBarColor);
	~Slider();

	bool mouseClick(int x, int y);

	void updatePos(int newPos);
	void sliderDown();
	void sliderUp();
	float sliderVal();

	vec2 topLeft;
	int sliderPos;

	vec3 color;
	vec3 downColor;
	vec3 upColor;

	vec3 barColor;

	int width;
};

#endif