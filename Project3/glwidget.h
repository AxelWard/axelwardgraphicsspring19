#ifndef __GLWIDGET__INCLUDE__
#define __GLWIDGET__INCLUDE__

#include <QGLWidget>
#include <QOpenGLWidget>
#include <QOpenGLFunctions_3_3_Core>
#include <QOpenGLTexture>
#include <QMouseEvent>
#include <QTimer>
#include <glm/glm.hpp>
#include <vector>

#include "menuItems/headers/panel.h"
#include "gameItems/headers/platformManager.h"
#include "gameItems/headers/player.h"
#include "gameItems/headers/winDiamond.h"
#include "gameItems/headers/wall.h"
#include "physics/headers/collisionDetector.h"
#include "physics/headers/bounding_box.h"

#define GLM_FORCE_RADIANS

using glm::mat4;
using glm::vec2;
using glm::vec3;

class GLWidget : public QOpenGLWidget, protected QOpenGLFunctions_3_3_Core {
	Q_OBJECT

public:
	GLWidget(QWidget *parent = 0);
	~GLWidget();

	GLuint loadShaders(const char* vertf, const char* fragf);

public slots:
	void animate();

protected:
	void initializeGL();
	void resizeGL(int w, int h);
	void paintGL();

	void mousePressEvent(QMouseEvent *event);
	void mouseMoveEvent(QMouseEvent *event);
	void mouseReleaseEvent(QMouseEvent *event);
	void keyPressEvent(QKeyEvent *event);
	void keyReleaseEvent(QKeyEvent *event);

	void valueUpdate(int index, float value);
	void updateView();

private:
	void initializeStaticTerrain();
	void renderStaticTerrain();

	void initializeStaticWalls();
	void renderStaticWalls();

	void initializeDynamicObjects();
	void renderDynamicObjects();

	void initializeMenu();
	void renderMenu();

	void initializeLife();
	void renderLife();

	QTimer frameTimer;

	// Platform Items
	GLuint staticTerrainProg;
	GLuint staticTerrainVao;

	GLuint staticTerrainPositionBuffer;
	GLuint staticTerrainColorBuffer;
	GLuint staticTerrainNormalBuffer;
	GLuint staticTerrainIndexBuffer;

	GLint staticTerrainPositionIndex;
	GLint staticTerrainColorIndex;
	GLint staticTerrainNormalIndex;

	GLint staticTerrainProjMatrixLoc;
	GLint staticTerrainViewMatrixLoc;
	GLint staticTerrainModelMatrixLoc;
	GLint staticTerrainLightLoc;
	GLint staticTerrainLightColorLoc;
	GLint staticTerrainLightIntenLoc;
	GLint staticTerrainShineLoc;

	mat4 staticTerrainProjMatrix;
	mat4 staticTerrainViewMatrix;
	mat4 camMatrix;
	mat4 staticTerrainModelMatrix;

	PlatformManager platforms;
	int numPlatforms;
	bool terrainUpdated;

	// Dynamic Items
	GLuint dynamicObjectProg;
	GLuint dynamicObjectVao;

	GLuint dynamicObjectPositionBuffer;
	GLuint dynamicObjectColorBuffer;
	GLuint dynamicObjectNormalBuffer;
	GLuint dynamicObjectIndexBuffer;

	GLint dynamicObjectPositionIndex;
	GLint dynamicObjectColorIndex;
	GLint dynamicObjectNormalIndex;

	GLint dynamicObjectProjMatrixLoc;
	GLint dynamicObjectViewMatrixLoc;
	GLint dynamicObjectModelMatrixLoc;
	GLint dynamicObjectLightLoc;
	GLint dynamicObjectLightColorLoc;
	GLint dynamicObjectLightIntenLoc;
	GLint dynamicObjectShineLoc;

	mat4 dynamicObjectProjMatrix;
	mat4 dynamicObjectViewMatrix;
	mat4 dynamicObjectModelMatrix;

	// Wall Items
	GLuint staticWallProg;
	GLuint staticWallVao;

	GLuint staticWallPositionBuffer;
	GLuint staticWallColorBuffer;
	GLuint staticWallNormalBuffer;
	GLuint staticWallIndexBuffer;

	GLint staticWallPositionIndex;
	GLint staticWallColorIndex;
	GLint staticWallNormalIndex;

	GLint staticWallProjMatrixLoc;
	GLint staticWallViewMatrixLoc;
	GLint staticWallModelMatrixLoc;
	GLint staticWallLightLoc;
	GLint staticWallLightColorLoc;
	GLint staticWallLightIntenLoc;
	GLint staticWallShineLoc;

	mat4 staticWallProjMatrix;
	mat4 staticWallViewMatrix;
	mat4 staticWallModelMatrix;

	int numWalls;

	// Menu Items
	GLuint menuProg;
	GLuint menuVao;
	GLuint menuPositionBuffer;
	GLuint menuColorBuffer;

	GLint menuProjectionMatrixLoc;
	mat4 menuOrtho;

	std::vector<vec2> menuPts;
	std::vector<vec3> menuColors;

	bool menuIsOpen;
	Panel menuPanel;
	int activeMenuItem;
	int width;
	int height;

	// Lifebar Items
	GLuint lifeProg;
	GLuint lifeVao;
	GLuint lifePositionBuffer;
	GLuint lifeColorBuffer;
	GLuint lifeTexCoordBuffer;
	GLuint lifeIndexBuffer;
	GLuint lifeTexObject;
	QOpenGLTexture lifeTex;

	GLint lifeProjectionMatrixLoc;
	mat4 lifeOrtho;

	int numLives;

	vec3 start;
	vec3 next;

	// Movement Items
	Player player;
	CollisionDetector cd;
	void checkPositions();

	// Level Items
	int currentLevel;
	WinDiamond currentWinDiamond;
	void loadLevel();
	vec3 spawnPos;
	vector<Wall> walls;
};

#endif
