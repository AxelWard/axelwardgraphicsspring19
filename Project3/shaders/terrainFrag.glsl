#version 330

uniform mat4 model;
uniform mat4 view;
uniform vec3 lightPos;
uniform vec3 lightCol;
uniform float lightIntensity;
uniform float shininess;

in vec3 fcolor;
in vec4 fnormal;
in vec4 fposition;

out vec4 color_out;

const float density = 0.08;
const float gradient = 1.5;

void main() {
  vec3 lightPosition = (model * vec4(lightPos, 0)).xyz;
  vec3 toLight = normalize(lightPosition - fposition.xyz);
  float brightness = clamp(dot(fnormal.xyz, toLight), 0, 1);
  vec3 diffuse = lightCol * brightness * lightIntensity;

  vec4 camPos = inverse(view) * vec4(0.0, 0.0, 0.0, 1.0);
  vec3 toCamera = normalize(camPos.xyz - fposition.xyz);
  vec3 fromLight = -toLight;
  vec3 reflected = reflect(fromLight, fnormal.xyz);
  vec3 specular = lightCol * pow(clamp(dot(reflected, toCamera), 0, 1), shininess) * lightIntensity;

  vec3 ambient = fcolor * .25;
  
  float distance = length(camPos.xyz - fposition.xyz);
  float visibility = exp(-pow((distance * density), gradient));
  visibility = clamp(visibility, 0.0, 1.0);

  vec4 out_Color = vec4(diffuse * fcolor + specular + ambient, 1);
  color_out = mix(vec4(0.52f, 0.81f, 1.0f, 1.0), out_Color, visibility);
  
}