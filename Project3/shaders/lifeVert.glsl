#version 330

uniform mat4 projection;

in vec3 position;
in vec3 color;
in vec2 texCoord;

out vec3 fcolor;
out vec2 ftexCoord;

void main() {
  gl_Position = projection * vec4(position, 1);
  fcolor = color;
  ftexCoord = texCoord;
}