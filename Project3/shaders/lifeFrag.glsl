#version 330

uniform sampler2D textureData;

in vec3 fcolor;
in vec2 ftexCoord;

out vec4 color_out;

void main() {
  vec4 color = texture(textureData, ftexCoord);
  color_out = color;
}