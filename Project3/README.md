## **Instructions for Use**


*Red Button* - Respawn at Beginning

*Green Slider* - Adjust Look Sensitivity

*Goal* - Get to the yellow diamond in each level without falling off three times. If you fall off 3 times you will restart.