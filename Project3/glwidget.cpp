#include "glwidget.h"
#include <iostream>

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <QTextStream>
#include <QImage>

#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

using glm::inverse;
using glm::vec2;
using glm::vec3;
using glm::mat4;
using glm::perspective;
using glm::normalize;
using glm::length;
using glm::cross;
using glm::dot;
using glm::rotate;
using glm::value_ptr;
using glm::lookAt;
using std::vector;

using namespace std;

GLWidget::GLWidget(QWidget *parent) : QOpenGLWidget(parent), lifeTex(QOpenGLTexture::Target2D) {

	menuPanel = Panel();
	activeMenuItem = -1;
	menuOrtho = glm::ortho(0.0f, 640.0f, 480.0f, 0.0f);
	lifeOrtho = glm::ortho(0.0f, 640.0f, 480.0f, 0.0f);
	numPlatforms = 0;
	numWalls = 0;
	staticTerrainModelMatrix = mat4(1.0f);
	dynamicObjectModelMatrix = mat4(1.0f);
	staticWallModelMatrix = mat4(1.0f);
	camMatrix = mat4(1.0f);
	menuIsOpen = false;

	frameTimer.callOnTimeout(this, &GLWidget::animate);
	frameTimer.start(16);
	
	player = Player();
	spawnPos = vec3(0.0f, 0.0f, 0.0f);
	cd = CollisionDetector();

	currentLevel = 1;
	currentWinDiamond = WinDiamond();

	numLives = 3;
}

GLWidget::~GLWidget() {
}

void GLWidget::initializeStaticTerrain() {
	glGenVertexArrays(1, &staticTerrainVao);
	glBindVertexArray(staticTerrainVao);

	glGenBuffers(1, &staticTerrainPositionBuffer);
	glGenBuffers(1, &staticTerrainColorBuffer);
	glGenBuffers(1, &staticTerrainNormalBuffer);
	glGenBuffers(1, &staticTerrainIndexBuffer);

	platforms = PlatformManager();

	loadLevel();

	vector<vec3> pts;
	vector<vec3> colors;
	vector<vec3> normals;
	vector<unsigned int> indices;

	platforms.getPositionData(pts);
	platforms.getColorData(colors);
	platforms.getNormalData(normals);
	platforms.getIndiceData(indices);

	glBindBuffer(GL_ARRAY_BUFFER, staticTerrainPositionBuffer);
	glBufferData(GL_ARRAY_BUFFER, pts.size() * sizeof(vec3), &pts[0], GL_DYNAMIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, staticTerrainColorBuffer);
	glBufferData(GL_ARRAY_BUFFER, colors.size() * sizeof(vec3), &colors[0], GL_DYNAMIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, staticTerrainNormalBuffer);
	glBufferData(GL_ARRAY_BUFFER, normals.size() * sizeof(vec3), &normals[0], GL_DYNAMIC_DRAW);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, staticTerrainIndexBuffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(unsigned int), &indices[0], GL_DYNAMIC_DRAW);

	GLuint program = loadShaders(":/shaders/terrainVert.glsl", ":/shaders/terrainFrag.glsl");
	glUseProgram(program);
	staticTerrainProg = program;

	glBindBuffer(GL_ARRAY_BUFFER, staticTerrainPositionBuffer);
	staticTerrainPositionIndex = glGetAttribLocation(program, "position");
	glEnableVertexAttribArray(staticTerrainPositionIndex);
	glVertexAttribPointer(staticTerrainPositionIndex, 3, GL_FLOAT, GL_FALSE, 0, 0);

	glBindBuffer(GL_ARRAY_BUFFER, staticTerrainColorBuffer);
	staticTerrainColorIndex = glGetAttribLocation(program, "color");
	glEnableVertexAttribArray(staticTerrainColorIndex);
	glVertexAttribPointer(staticTerrainColorIndex, 3, GL_FLOAT, GL_FALSE, 0, 0);

	glBindBuffer(GL_ARRAY_BUFFER, staticTerrainNormalBuffer);
	staticTerrainNormalIndex = glGetAttribLocation(program, "normal");
	glEnableVertexAttribArray(staticTerrainNormalIndex);
	glVertexAttribPointer(staticTerrainNormalIndex, 3, GL_FLOAT, GL_FALSE, 0, 0);

	staticTerrainProjMatrixLoc = glGetUniformLocation(program, "projection");
	staticTerrainViewMatrixLoc = glGetUniformLocation(program, "view");
	staticTerrainModelMatrixLoc = glGetUniformLocation(program, "model");
	staticTerrainLightLoc = glGetUniformLocation(program, "lightPos");
	staticTerrainLightColorLoc = glGetUniformLocation(program, "lightCol");
	staticTerrainLightIntenLoc = glGetUniformLocation(program, "lightIntensity");
	staticTerrainShineLoc = glGetUniformLocation(program, "shininess");

	terrainUpdated = true;
}

void GLWidget::initializeStaticWalls() {
	glGenVertexArrays(1, &staticWallVao);
	glBindVertexArray(staticWallVao);

	glGenBuffers(1, &staticWallPositionBuffer);
	glGenBuffers(1, &staticWallColorBuffer);
	glGenBuffers(1, &staticWallNormalBuffer);
	glGenBuffers(1, &staticWallIndexBuffer);

	vector<vec3> pts;
	vector<vec3> colors;
	vector<vec3> normals;
	vector<unsigned int> indices;

	for (int i = 0; i < numWalls; i++) {
		walls[i].getPositions(pts);
		walls[i].getColors(colors);
		walls[i].getNormals(normals);
		walls[i].getIndices(indices, i*36);
	}

	glBindBuffer(GL_ARRAY_BUFFER, staticWallPositionBuffer);
	glBufferData(GL_ARRAY_BUFFER, pts.size() * sizeof(vec3), &pts[0], GL_DYNAMIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, staticWallColorBuffer);
	glBufferData(GL_ARRAY_BUFFER, colors.size() * sizeof(vec3), &colors[0], GL_DYNAMIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, staticWallNormalBuffer);
	glBufferData(GL_ARRAY_BUFFER, normals.size() * sizeof(vec3), &normals[0], GL_DYNAMIC_DRAW);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, staticWallIndexBuffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(unsigned int), &indices[0], GL_DYNAMIC_DRAW);

	GLuint program = loadShaders(":/shaders/terrainVert.glsl", ":/shaders/terrainFrag.glsl");
	glUseProgram(program);
	staticWallProg = program;

	glBindBuffer(GL_ARRAY_BUFFER, staticWallPositionBuffer);
	staticWallPositionIndex = glGetAttribLocation(program, "position");
	glEnableVertexAttribArray(staticWallPositionIndex);
	glVertexAttribPointer(staticWallPositionIndex, 3, GL_FLOAT, GL_FALSE, 0, 0);

	glBindBuffer(GL_ARRAY_BUFFER, staticWallColorBuffer);
	staticWallColorIndex = glGetAttribLocation(program, "color");
	glEnableVertexAttribArray(staticWallColorIndex);
	glVertexAttribPointer(staticWallColorIndex, 3, GL_FLOAT, GL_FALSE, 0, 0);

	glBindBuffer(GL_ARRAY_BUFFER, staticWallNormalBuffer);
	staticWallNormalIndex = glGetAttribLocation(program, "normal");
	glEnableVertexAttribArray(staticWallNormalIndex);
	glVertexAttribPointer(staticWallNormalIndex, 3, GL_FLOAT, GL_FALSE, 0, 0);

	staticWallProjMatrixLoc = glGetUniformLocation(program, "projection");
	staticWallViewMatrixLoc = glGetUniformLocation(program, "view");
	staticWallModelMatrixLoc = glGetUniformLocation(program, "model");
	staticWallLightLoc = glGetUniformLocation(program, "lightPos");
	staticWallLightColorLoc = glGetUniformLocation(program, "lightCol");
	staticWallLightIntenLoc = glGetUniformLocation(program, "lightIntensity");
	staticWallShineLoc = glGetUniformLocation(program, "shininess");
}

void GLWidget::initializeDynamicObjects() {
	glGenVertexArrays(1, &dynamicObjectVao);
	glBindVertexArray(dynamicObjectVao);

	glGenBuffers(1, &dynamicObjectPositionBuffer);
	glGenBuffers(1, &dynamicObjectColorBuffer);
	glGenBuffers(1, &dynamicObjectNormalBuffer);
	glGenBuffers(1, &dynamicObjectIndexBuffer);

	vector<vec3> pts;
	vector<vec3> colors;
	vector<vec3> normals;
	vector<unsigned int> indices;

	currentWinDiamond.getPositions(pts);
	currentWinDiamond.getColors(colors);
	currentWinDiamond.getNormals(normals);
	currentWinDiamond.getIndices(indices, 0);

	glBindBuffer(GL_ARRAY_BUFFER, dynamicObjectPositionBuffer);
	glBufferData(GL_ARRAY_BUFFER, pts.size() * sizeof(vec3), &pts[0], GL_DYNAMIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, dynamicObjectColorBuffer);
	glBufferData(GL_ARRAY_BUFFER, colors.size() * sizeof(vec3), &colors[0], GL_DYNAMIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, dynamicObjectNormalBuffer);
	glBufferData(GL_ARRAY_BUFFER, normals.size() * sizeof(vec3), &normals[0], GL_DYNAMIC_DRAW);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, dynamicObjectIndexBuffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(unsigned int), &indices[0], GL_DYNAMIC_DRAW);

	GLuint program = loadShaders(":/shaders/terrainVert.glsl", ":/shaders/terrainFrag.glsl");
	glUseProgram(program);
	dynamicObjectProg = program;

	glBindBuffer(GL_ARRAY_BUFFER, dynamicObjectPositionBuffer);
	dynamicObjectPositionIndex = glGetAttribLocation(program, "position");
	glEnableVertexAttribArray(dynamicObjectPositionIndex);
	glVertexAttribPointer(dynamicObjectPositionIndex, 3, GL_FLOAT, GL_FALSE, 0, 0);

	glBindBuffer(GL_ARRAY_BUFFER, dynamicObjectColorBuffer);
	dynamicObjectColorIndex = glGetAttribLocation(program, "color");
	glEnableVertexAttribArray(dynamicObjectColorIndex);
	glVertexAttribPointer(dynamicObjectColorIndex, 3, GL_FLOAT, GL_FALSE, 0, 0);

	glBindBuffer(GL_ARRAY_BUFFER, dynamicObjectNormalBuffer);
	dynamicObjectNormalIndex = glGetAttribLocation(program, "normal");
	glEnableVertexAttribArray(dynamicObjectNormalIndex);
	glVertexAttribPointer(dynamicObjectNormalIndex, 3, GL_FLOAT, GL_FALSE, 0, 0);

	dynamicObjectProjMatrixLoc = glGetUniformLocation(program, "projection");
	dynamicObjectViewMatrixLoc = glGetUniformLocation(program, "view");
	dynamicObjectModelMatrixLoc = glGetUniformLocation(program, "model");
	dynamicObjectLightLoc = glGetUniformLocation(program, "lightPos");
	dynamicObjectLightColorLoc = glGetUniformLocation(program, "lightCol");
	dynamicObjectLightIntenLoc = glGetUniformLocation(program, "lightIntensity");
	dynamicObjectShineLoc = glGetUniformLocation(program, "shininess");
}

void GLWidget::initializeMenu() {

	menuPanel.addButton(vec2(10, 0), 50, 50, vec3(1, 0, 0)); //  respawn
	menuPanel.addSlider(vec2(10, 60), 100, vec3(.9, .9, .9), vec3(0, 1, 0)); //look sensitivity

	glGenVertexArrays(1, &menuVao);
	glBindVertexArray(menuVao);

	glGenBuffers(1, &menuPositionBuffer);
	glGenBuffers(1, &menuColorBuffer);

	glBindBuffer(GL_ARRAY_BUFFER, menuPositionBuffer);
	glBufferData(GL_ARRAY_BUFFER, 0, NULL, GL_DYNAMIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, menuColorBuffer);
	glBufferData(GL_ARRAY_BUFFER, 0, NULL, GL_DYNAMIC_DRAW);

	GLuint program = loadShaders(":/shaders/menuVert.glsl", ":/shaders/menuFrag.glsl");
	glUseProgram(program);
	menuProg = program;

	glBindBuffer(GL_ARRAY_BUFFER, menuPositionBuffer);
	GLint positionIndex = glGetAttribLocation(program, "position");
	glEnableVertexAttribArray(positionIndex);
	glVertexAttribPointer(positionIndex, 2, GL_FLOAT, GL_FALSE, 0, 0);

	glBindBuffer(GL_ARRAY_BUFFER, menuColorBuffer);
	GLint colorIndex = glGetAttribLocation(program, "colorIn");
	glEnableVertexAttribArray(colorIndex);
	glVertexAttribPointer(colorIndex, 3, GL_FLOAT, GL_FALSE, 0, 0);

	menuProjectionMatrixLoc = glGetUniformLocation(program, "ProjectionMatrix");

	glUniformMatrix4fv(menuProjectionMatrixLoc, 1, GL_FALSE, glm::value_ptr(menuOrtho));
}

void GLWidget::initializeLife() {
	glGenVertexArrays(1, &lifeVao);
	glBindVertexArray(lifeVao);

	glGenBuffers(1, &lifePositionBuffer);
	glGenBuffers(1, &lifeColorBuffer);
	glGenBuffers(1, &lifeTexCoordBuffer);
	glGenBuffers(1, &lifeIndexBuffer);


	vec3 pts[] = {
		vec3(0,0,0),   // 0
		vec3(50,0,0),  // 1
		vec3(0,50,0),  // 2
		vec3(50,50,0), // 3

		vec3(50,0,0),   // 4
		vec3(100,0,0),  // 5
		vec3(50,50,0),  // 6
		vec3(100,50,0), // 7

		vec3(100,0,0),  // 8
		vec3(150,0,0),  // 9
		vec3(100,50,0), // 10
		vec3(150,50,0)  // 11
	};

	vec3 colors[] = {
		vec3(0,1,0),
		vec3(0,1,0),
		vec3(0,1,0),
		vec3(0,1,0),

		vec3(0,1,0),
		vec3(0,1,0),
		vec3(0,1,0),
		vec3(0,1,0),

		vec3(0,1,0),
		vec3(0,1,0),
		vec3(0,1,0),
		vec3(0,1,0)
	};

	vec2 texCoords[] = {
		vec2(0, 1),
		vec2(1, 1),
		vec2(0, 0),
		vec2(1, 0),

		vec2(0, 1),
		vec2(1, 1),
		vec2(0, 0),
		vec2(1, 0),

		vec2(0, 1),
		vec2(1, 1),
		vec2(0, 0),
		vec2(1, 0)
	};

	GLuint indices[] = {
		0,1,2,
		1,2,3,
		4,5,6,
		5,6,7,
		8,9,10,
		9,10,11
	};

	glBindBuffer(GL_ARRAY_BUFFER, lifePositionBuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(pts), pts, GL_STATIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, lifeColorBuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(colors), colors, GL_STATIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, lifeTexCoordBuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(texCoords), texCoords, GL_STATIC_DRAW);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, lifeIndexBuffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

	GLuint program = loadShaders(":/shaders/lifeVert.glsl", ":/shaders/lifeFrag.glsl");
	glUseProgram(program);
	lifeProg = program;

	glBindBuffer(GL_ARRAY_BUFFER, lifePositionBuffer);
	GLint positionIndex = glGetAttribLocation(program, "position");
	glEnableVertexAttribArray(positionIndex);
	glVertexAttribPointer(positionIndex, 3, GL_FLOAT, GL_FALSE, 0, 0);

	glBindBuffer(GL_ARRAY_BUFFER, lifeColorBuffer);
	GLint colorIndex = glGetAttribLocation(program, "color");
	glEnableVertexAttribArray(colorIndex);
	glVertexAttribPointer(colorIndex, 3, GL_FLOAT, GL_FALSE, 0, 0);

	glBindBuffer(GL_ARRAY_BUFFER, lifeTexCoordBuffer);
	GLint texCoordIndex = glGetAttribLocation(program, "texCoord");
	glEnableVertexAttribArray(texCoordIndex);
	glVertexAttribPointer(texCoordIndex, 2, GL_FLOAT, GL_FALSE, 0, 0);

	lifeProjectionMatrixLoc = glGetUniformLocation(program, "projection");
}

void GLWidget::initializeGL() {
	initializeOpenGLFunctions();

	glClearColor(0.52f, 0.81f, 1.0f, 1.0f);
	glPointSize(4.0f);

	glEnable(GL_DEPTH_TEST);

	glGenTextures(1, &lifeTexObject);

	glBindTexture(GL_TEXTURE_2D, lifeTexObject);
	QImage img = QImage(":/heart.png").convertToFormat(QImage::Format_RGBA8888);

	lifeTex.setData(img.mirrored());
	lifeTex.bind();

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	initializeStaticTerrain();
	initializeStaticWalls();
	initializeDynamicObjects();
	initializeMenu();
	initializeLife();
}

void GLWidget::resizeGL(int w, int h) {
	width = w;
	height = h;

	float aspect = (float)w / h;

	staticTerrainProjMatrix = perspective(45.0f, aspect, .01f, 100.0f);
	dynamicObjectProjMatrix = perspective(45.0f, aspect, .01f, 100.0f);
	staticWallProjMatrix = perspective(45.0f, aspect, .01f, 100.0f);
	
	menuOrtho = glm::ortho(0.0f, (float) w, (float) h, 0.0f);
	lifeOrtho = glm::ortho(0.0f, (float)w, (float)h, 0.0f);

	float lightPos[]{ 50, 100, 50};
	float lightColor[]{ 1, 1, .9 };
	float lightIntensity = 1;
	float shininess = 50;

	glUseProgram(staticTerrainProg);
	glUniformMatrix4fv(staticTerrainProjMatrixLoc, 1, false, value_ptr(staticTerrainProjMatrix));
	glUniformMatrix4fv(staticTerrainViewMatrixLoc, 1, false, value_ptr(staticTerrainViewMatrix));
	glUniformMatrix4fv(staticTerrainModelMatrixLoc, 1, false, value_ptr(staticTerrainModelMatrix));
	glUniform3fv(staticTerrainLightLoc, 1, lightPos);
	glUniform3fv(staticTerrainLightColorLoc, 1, lightColor);
	glUniform1f(staticTerrainLightIntenLoc, lightIntensity);
	glUniform1f(staticTerrainShineLoc, shininess);

	glUseProgram(staticWallProg);
	glUniformMatrix4fv(staticWallProjMatrixLoc, 1, false, value_ptr(staticWallProjMatrix));
	glUniformMatrix4fv(staticWallViewMatrixLoc, 1, false, value_ptr(staticWallViewMatrix));
	glUniformMatrix4fv(staticWallModelMatrixLoc, 1, false, value_ptr(staticWallModelMatrix));
	glUniform3fv(staticWallLightLoc, 1, lightPos);
	glUniform3fv(staticWallLightColorLoc, 1, lightColor);
	glUniform1f(staticWallLightIntenLoc, lightIntensity);
	glUniform1f(staticWallShineLoc, shininess - 40);

	glUseProgram(dynamicObjectProg);
	glUniformMatrix4fv(dynamicObjectProjMatrixLoc, 1, false, value_ptr(dynamicObjectProjMatrix));
	glUniformMatrix4fv(dynamicObjectViewMatrixLoc, 1, false, value_ptr(dynamicObjectViewMatrix));
	glUniformMatrix4fv(dynamicObjectModelMatrixLoc, 1, false, value_ptr(dynamicObjectModelMatrix));
	glUniform3fv(dynamicObjectLightLoc, 1, lightPos);
	glUniform3fv(dynamicObjectLightColorLoc, 1, lightColor);
	glUniform1f(dynamicObjectLightIntenLoc, lightIntensity);
	glUniform1f(dynamicObjectShineLoc, shininess + 50);

	glUseProgram(menuProg);
	glUniformMatrix4fv(menuProjectionMatrixLoc, 1, GL_FALSE, glm::value_ptr(menuOrtho));

	glUseProgram(lifeProg);
	glUniformMatrix4fv(menuProjectionMatrixLoc, 1, GL_FALSE, glm::value_ptr(lifeOrtho));
}

void GLWidget::paintGL() {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	if (terrainUpdated) {
		renderStaticTerrain();
		renderStaticWalls();
		terrainUpdated = false;
	}

	glUseProgram(staticTerrainProg);
	glBindVertexArray(staticTerrainVao);
	glDrawElements(GL_TRIANGLES, numPlatforms * 36, GL_UNSIGNED_INT, 0);

	glUseProgram(staticWallProg);
	glBindVertexArray(staticWallVao);
	glDrawElements(GL_TRIANGLES, numWalls * 36, GL_UNSIGNED_INT, 0);

	renderDynamicObjects();
	if (menuIsOpen) {
		renderMenu();
	}
	else {
		renderLife();
	}
}

void GLWidget::renderStaticTerrain() {
	glUseProgram(staticTerrainProg);
	glBindVertexArray(staticTerrainVao);

	vector<vec3> pts;
	vector<vec3> colors;
	vector<vec3> normals;
	vector<unsigned int> indices;

	platforms.getPositionData(pts);
	platforms.getColorData(colors);
	platforms.getNormalData(normals);
	platforms.getIndiceData(indices);

	glBindBuffer(GL_ARRAY_BUFFER, staticTerrainPositionBuffer);
	glBufferData(GL_ARRAY_BUFFER, pts.size() * sizeof(vec3), &pts[0], GL_DYNAMIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, staticTerrainColorBuffer);
	glBufferData(GL_ARRAY_BUFFER, colors.size() * sizeof(vec3), &colors[0], GL_DYNAMIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, staticTerrainNormalBuffer);
	glBufferData(GL_ARRAY_BUFFER, normals.size() * sizeof(vec3), &normals[0], GL_DYNAMIC_DRAW);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, staticTerrainIndexBuffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(unsigned int), &indices[0], GL_DYNAMIC_DRAW);
}

void GLWidget::renderStaticWalls() {
	glUseProgram(staticWallProg);
	glBindVertexArray(staticWallVao);

	vector<vec3> pts;
	vector<vec3> colors;
	vector<vec3> normals;
	vector<unsigned int> indices;

	for (int i = 0; i < numWalls; i++) {
		walls[i].getPositions(pts);
		walls[i].getColors(colors);
		walls[i].getNormals(normals);
		walls[i].getIndices(indices, i * 36);
	}

	glBindBuffer(GL_ARRAY_BUFFER, staticWallPositionBuffer);
	glBufferData(GL_ARRAY_BUFFER, pts.size() * sizeof(vec3), &pts[0], GL_DYNAMIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, staticWallColorBuffer);
	glBufferData(GL_ARRAY_BUFFER, colors.size() * sizeof(vec3), &colors[0], GL_DYNAMIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, staticWallNormalBuffer);
	glBufferData(GL_ARRAY_BUFFER, normals.size() * sizeof(vec3), &normals[0], GL_DYNAMIC_DRAW);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, staticWallIndexBuffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(unsigned int), &indices[0], GL_DYNAMIC_DRAW);
}

void GLWidget::renderDynamicObjects() {
	glUseProgram(dynamicObjectProg);
	glBindVertexArray(dynamicObjectVao);

	vector<vec3> pts;
	vector<vec3> colors;
	vector<vec3> normals;
	vector<unsigned int> indices;

	currentWinDiamond.getPositions(pts);
	currentWinDiamond.getColors(colors);
	currentWinDiamond.getNormals(normals);
	currentWinDiamond.getIndices(indices, 0);

	glBindBuffer(GL_ARRAY_BUFFER, dynamicObjectPositionBuffer);
	glBufferData(GL_ARRAY_BUFFER, pts.size() * sizeof(vec3), &pts[0], GL_DYNAMIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, dynamicObjectColorBuffer);
	glBufferData(GL_ARRAY_BUFFER, colors.size() * sizeof(vec3), &colors[0], GL_DYNAMIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, dynamicObjectNormalBuffer);
	glBufferData(GL_ARRAY_BUFFER, normals.size() * sizeof(vec3), &normals[0], GL_DYNAMIC_DRAW);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, dynamicObjectIndexBuffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(unsigned int), &indices[0], GL_DYNAMIC_DRAW);

	glUseProgram(dynamicObjectProg);
	glBindVertexArray(dynamicObjectVao);
	glDrawElements(GL_TRIANGLES, 24, GL_UNSIGNED_INT, 0);
}

void GLWidget::renderMenu() {
	glUseProgram(menuProg);
	glBindVertexArray(menuVao);

	int num_buttons = menuPanel.numButtons;
	int num_sliders = menuPanel.numSliders;

	for (int i = 0; i < num_buttons; i++) {

		vec2 start = menuPanel.getButtonStart(i);
		int w = menuPanel.getButtonWidth(i);
		int h = menuPanel.getButtonHeight(i);
		vec3 color = menuPanel.getButtonColor(i);

		menuPts.push_back(start);
		menuColors.push_back(color);

		menuPts.push_back(vec2(start.x, start.y + h));
		menuColors.push_back(color);

		menuPts.push_back(vec2(start.x + w, start.y));
		menuColors.push_back(color);

		menuPts.push_back(vec2(start.x, start.y + h));
		menuColors.push_back(color);

		menuPts.push_back(vec2(start.x + w, start.y + h));
		menuColors.push_back(color);

		menuPts.push_back(vec2(start.x + w, start.y));
		menuColors.push_back(color);
	}

	for (int i = 0; i < num_sliders; i++) {

		vec2 start = menuPanel.getSliderStart(i);
		int w = menuPanel.getSliderWidth(i);
		int h = 25;
		vec3 barColor = menuPanel.getSliderBarColor(i);
		vec3 color = menuPanel.getSliderColor(i);
		int pos = menuPanel.getSliderPos(i);

		//button

		menuPts.push_back(vec2(pos - 5, start.y));
		menuColors.push_back(color);

		menuPts.push_back(vec2(pos - 5, start.y + h));
		menuColors.push_back(color);

		menuPts.push_back(vec2(pos + 5, start.y));
		menuColors.push_back(color);

		menuPts.push_back(vec2(pos + 5, start.y));
		menuColors.push_back(color);

		menuPts.push_back(vec2(pos + 5, start.y + h));
		menuColors.push_back(color);

		menuPts.push_back(vec2(pos - 5, start.y + h));
		menuColors.push_back(color);

		//bar

		menuPts.push_back(vec2(start.x, start.y + 11));
		menuColors.push_back(barColor);

		menuPts.push_back(vec2(start.x, start.y + 15));
		menuColors.push_back(barColor);

		menuPts.push_back(vec2(start.x + w, start.y + 15));
		menuColors.push_back(barColor);

		menuPts.push_back(vec2(start.x, start.y + 11));
		menuColors.push_back(barColor);

		menuPts.push_back(vec2(start.x + w, start.y + 15));
		menuColors.push_back(barColor);

		menuPts.push_back(vec2(start.x + w, start.y + 11));
		menuColors.push_back(barColor);
	}

	glBindBuffer(GL_ARRAY_BUFFER, menuPositionBuffer);
	glBufferData(GL_ARRAY_BUFFER, menuPts.size() * sizeof(vec2), &menuPts[0], GL_DYNAMIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, menuColorBuffer);
	glBufferData(GL_ARRAY_BUFFER, menuColors.size() * sizeof(vec3), &menuColors[0], GL_DYNAMIC_DRAW);

	glDrawArrays(GL_TRIANGLES, 0, ((menuPanel.numButtons * 6) + (menuPanel.numSliders *12)));
	
	menuPts.clear();
	menuColors.clear();
}

void GLWidget::renderLife() {
	glUseProgram(lifeProg);
	glBindVertexArray(lifeVao);

	glBindTexture(GL_TEXTURE_2D, lifeTexObject);
	lifeTex.bind();

	glDrawElements(GL_TRIANGLES, 6 * numLives, GL_UNSIGNED_INT, 0);
}

GLuint GLWidget::loadShaders(const char* vertf, const char* fragf) {
	GLuint program = glCreateProgram();

	// read vertex shader from Qt resource file
	QFile vertFile(vertf);
	vertFile.open(QFile::ReadOnly | QFile::Text);
	QString vertString;
	QTextStream vertStream(&vertFile);
	vertString.append(vertStream.readAll());
	std::string vertSTLString = vertString.toStdString();

	const GLchar* vertSource = vertSTLString.c_str();

	GLuint vertShader = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vertShader, 1, &vertSource, NULL);
	glCompileShader(vertShader);
	{
		GLint compiled;
		glGetShaderiv(vertShader, GL_COMPILE_STATUS, &compiled);
		if (!compiled) {
			GLsizei len;
			glGetShaderiv(vertShader, GL_INFO_LOG_LENGTH, &len);

			GLchar* log = new GLchar[len + 1];
			glGetShaderInfoLog(vertShader, len, &len, log);
			std::cout << "Shader compilation failed: " << log << std::endl;
			delete[] log;
		}
	}
	glAttachShader(program, vertShader);

	// read fragment shader from Qt resource file
	QFile fragFile(fragf);
	fragFile.open(QFile::ReadOnly | QFile::Text);
	QString fragString;
	QTextStream fragStream(&fragFile);
	fragString.append(fragStream.readAll());
	std::string fragSTLString = fragString.toStdString();

	const GLchar* fragSource = fragSTLString.c_str();

	GLuint fragShader = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragShader, 1, &fragSource, NULL);
	glCompileShader(fragShader);
	{
		GLint compiled;
		glGetShaderiv(fragShader, GL_COMPILE_STATUS, &compiled);
		if (!compiled) {
			GLsizei len;
			glGetShaderiv(fragShader, GL_INFO_LOG_LENGTH, &len);

			GLchar* log = new GLchar[len + 1];
			glGetShaderInfoLog(fragShader, len, &len, log);
			std::cerr << "Shader compilation failed: " << log << std::endl;
			delete[] log;
		}
	}
	glAttachShader(program, fragShader);

	glLinkProgram(program);

	return program;
}

void GLWidget::mousePressEvent(QMouseEvent *event) {
	if (!menuIsOpen) {
		vec2 pt(event->x(), event->y());
		player.startView(pt);
		activeMenuItem = -1;
	}
	else if (menuIsOpen) {
		activeMenuItem = menuPanel.mouseClick(event->x(), event->y());

		int num_buttons = menuPanel.numButtons;
		menuPanel.setMenuItemDown(activeMenuItem);

		if (activeMenuItem == 0) {
			player.setPos(spawnPos, camMatrix);
		}
		else if (activeMenuItem > 0) {
			int mouseX = event->x();
			int activeSlider = activeMenuItem - num_buttons;

			if (mouseX > menuPanel.getSliderStart(activeSlider).x && mouseX < menuPanel.getSliderStart(activeSlider).x + menuPanel.getSliderWidth(activeSlider)) {
				menuPanel.setSliderPos(activeSlider, mouseX);
				valueUpdate(activeSlider, menuPanel.getSliderValue(activeSlider));
			}
			else if (mouseX < menuPanel.getSliderStart(activeSlider).x) {
				menuPanel.setSliderPos(activeSlider, menuPanel.getSliderStart(activeSlider).x);
				valueUpdate(activeSlider, menuPanel.getSliderValue(activeSlider));
			}
			else if (mouseX > menuPanel.getSliderStart(activeSlider).x + menuPanel.getSliderWidth(activeSlider)) {
				menuPanel.setSliderPos(activeSlider, menuPanel.getSliderStart(activeSlider).x + menuPanel.getSliderWidth(activeSlider));
				valueUpdate(activeSlider, menuPanel.getSliderValue(activeSlider));
			}
		}

		update();
	}
}

void GLWidget::mouseMoveEvent(QMouseEvent *event) {
	int num_buttons = menuPanel.numButtons;
	if (activeMenuItem == -1 && !menuIsOpen) {
		player.updateView(vec2(event->x(), event->y()), camMatrix);
		updateView();
	}
	else if (activeMenuItem > 0) {
		int mouseX = event->x();
		int activeSlider = activeMenuItem - num_buttons;

		if (mouseX > menuPanel.getSliderStart(activeSlider).x && mouseX < menuPanel.getSliderStart(activeSlider).x + menuPanel.getSliderWidth(activeSlider)) {
			menuPanel.setSliderPos(activeSlider, mouseX);
			valueUpdate(activeSlider, menuPanel.getSliderValue(activeSlider));
		}
		else if (mouseX < menuPanel.getSliderStart(activeSlider).x) {
			menuPanel.setSliderPos(activeSlider, menuPanel.getSliderStart(activeSlider).x);
			valueUpdate(activeSlider, menuPanel.getSliderValue(activeSlider));
		}
		else if (mouseX > menuPanel.getSliderStart(activeSlider).x + menuPanel.getSliderWidth(activeSlider)) {
			menuPanel.setSliderPos(activeSlider, menuPanel.getSliderStart(activeSlider).x + menuPanel.getSliderWidth(activeSlider));
			valueUpdate(activeSlider, menuPanel.getSliderValue(activeSlider));
		}

		update();
	}
}

void GLWidget::valueUpdate(int index, float value) {
	if (index == 0) {
		player.sensitivity = -value - .1;
	}
}

void GLWidget::mouseReleaseEvent(QMouseEvent *event) {
	if (activeMenuItem != -1) {
		menuPanel.setMenuItemUp(activeMenuItem);
	}
}

void GLWidget::keyPressEvent(QKeyEvent *event) {
	switch (event->key()) {
	case Qt::Key_W:
		if (!menuIsOpen) {
			player.forward = true;
		}
		break;
	case Qt::Key_A:
		if (!menuIsOpen) {
			player.left = true;
		}
		break;
	case Qt::Key_S:
		if (!menuIsOpen) {
			player.backward = true;
		}
		break;
	case Qt::Key_D:
		if (!menuIsOpen) {
			player.right = true;
		}
		break;
	case Qt::Key_Space:
		if (!menuIsOpen) {
			player.space = true;
		}
		break;
	case Qt::Key_Shift:
		if (!menuIsOpen) {
			player.shift = true;
		}
		break;
	case Qt::Key_Escape:
		menuIsOpen = !menuIsOpen;
	}
}

void GLWidget::keyReleaseEvent(QKeyEvent *event) {
	switch (event->key()) {
	case Qt::Key_W:
		player.forward = false;
		break;
	case Qt::Key_A:
		player.left = false;
		break;
	case Qt::Key_S:
		player.backward = false;
		break;
	case Qt::Key_D:
		player.right = false;
		break;
	case Qt::Key_Space:
		player.space = false;
		break;
	case Qt::Key_Shift:
		player.shift = false;
		break;
	}
}

void GLWidget::updateView() {
	staticTerrainViewMatrix = inverse(camMatrix);
	dynamicObjectViewMatrix = inverse(camMatrix);
	staticWallViewMatrix = inverse(camMatrix);

	glUseProgram(staticTerrainProg);
	glUniformMatrix4fv(staticTerrainViewMatrixLoc, 1, false, value_ptr(staticTerrainViewMatrix));

	glUseProgram(staticWallProg);
	glUniformMatrix4fv(staticWallViewMatrixLoc, 1, false, value_ptr(staticWallViewMatrix));

	glUseProgram(dynamicObjectProg);
	glUniformMatrix4fv(dynamicObjectViewMatrixLoc, 1, false, value_ptr(dynamicObjectViewMatrix));
}

void GLWidget::animate() {
	checkPositions();

	player.updatePos(camMatrix);
	currentWinDiamond.updatePos(.16);

	updateView();
	update();
}

void GLWidget::checkPositions() {
	player.updateGroundLevel(-10.0f);
	player.resetVelocityMultiplier();
	for (int i = 0; i < numPlatforms; i++) {
		if (cd.checkBBoxes(player.bBox, platforms.platforms[i].bBox)) {
			cd.getPlayerDirection(player, platforms.platforms[i].bBox);
		}
	}

	for (int i = 0; i < numWalls; i++) {
		if (cd.checkBBoxes(player.bBox, walls[i].bBox)) {
			cd.getPlayerDirection(player, walls[i].bBox);
		}
	}

	if (cd.checkBBoxes(player.bBox, currentWinDiamond.bBox)) {
		currentLevel += 1;
		loadLevel();
	}

	if (player.getPos().y < -5) {
		numLives -= 1;

		if (numLives == 0) {
			numLives = 3;
			currentLevel = 1;
			loadLevel();
		}
		else {
			player.setPos(spawnPos, camMatrix);
		}
	}
}

void GLWidget::loadLevel() {
	platforms.clearPlatforms();
	numPlatforms = 0;
	if (currentLevel == 1) {
		spawnPos = vec3(0.0f, 3.0f, 0.0f);

		platforms.createLevel(0.5f, 3.0f, 2, 2);
		numPlatforms = 5;

		numWalls = 1;
		walls.push_back(Wall(vec3(-10,0,1), vec3(10,5,1), vec3(0,0,1)));

		vec3 finalPlat = platforms.getPlatformLocation(4);
		finalPlat.y += 1;
		currentWinDiamond.updateCenter(finalPlat);
	}
	else if (currentLevel == 2) {
		walls.clear();
		numWalls = 0;
		spawnPos = vec3(0.0f, 3.0f, 0.0f);

		platforms.createLevel(1.0f, 3.0f, 3, 3);
		numPlatforms = 10;

		numWalls = 1;
		walls.push_back(Wall(vec3(-12, 0, -9), vec3(1, 8, 11), vec3(1, 0, 1)));

		vec3 finalPlat = platforms.getPlatformLocation(9);
		finalPlat.y += 1;
		currentWinDiamond.updateCenter(finalPlat);
	}
	else if (currentLevel == 3) {
		walls.clear();
		numWalls = 0;
		spawnPos = vec3(0.0f, 3.0f, 0.0f);

		platforms.createLevel(0.5f, 4.0f, 4, 3);
		numPlatforms = 13;

		numWalls = 1;
		walls.push_back(Wall(vec3(-12, 0, -9), vec3(1, 8, 11), vec3(1, 0, 1)));

		vec3 finalPlat = platforms.getPlatformLocation(12);
		finalPlat.y += 1;
		currentWinDiamond.updateCenter(finalPlat);
	}
	else if (currentLevel == 4) {
		walls.clear();
		numWalls = 0;
		spawnPos = vec3(0.0f, 3.0f, 0.0f);

		platforms.createLevel(1.0f, 4.0f, 3, 4);
		numPlatforms = 13;

		numWalls = 1;
		walls.push_back(Wall(vec3(-10.75, -1, -10), vec3(0, 15, 10), vec3(0, 1, 0)));

		vec3 finalPlat = platforms.getPlatformLocation(12);
		finalPlat.y += 1;
		currentWinDiamond.updateCenter(finalPlat);
	}
	else if (currentLevel == 5) {
		walls.clear();
		numWalls = 0;
		spawnPos = vec3(0.0f, 3.0f, 0.0f);

		platforms.createLevel(0.5f, 5.0f, 4, 5);
		numPlatforms = 21;

		numWalls = 1;
		walls.push_back(Wall(vec3(-10.75, -1, -10), vec3(0, 15, 10), vec3(0, 1, 1)));

		vec3 finalPlat = platforms.getPlatformLocation(20);
		finalPlat.y += 1;
		currentWinDiamond.updateCenter(finalPlat);
	}
	else if (currentLevel == 6) {
		walls.clear();
		numWalls = 0;
		spawnPos = vec3(0.0f, 3.0f, 0.0f);

		platforms.createLevel(1.0f, 5.0f, 5, 4);
		numPlatforms = 21;

		numWalls = 1;
		walls.push_back(Wall(vec3(-10.75, -1, -10), vec3(0, 15, 10), vec3(0, 1, 0)));

		vec3 finalPlat = platforms.getPlatformLocation(20);
		finalPlat.y += 1;
		currentWinDiamond.updateCenter(finalPlat);
	}
	else if (currentLevel == 7) {
		walls.clear();
		numWalls = 0;
		spawnPos = vec3(0.0f, 3.0f, 0.0f);

		platforms.createLevel(0.5f, 5.5f, 5, 5);
		numPlatforms = 26;

		numWalls = 1;
		walls.push_back(Wall(vec3(-10.75, -1, -10), vec3(0, 15, 10), vec3(1, 1, 0)));

		vec3 finalPlat = platforms.getPlatformLocation(25);
		finalPlat.y += 1;
		currentWinDiamond.updateCenter(finalPlat);
	}
	else if (currentLevel == 8) {
		walls.clear();
		numWalls = 0;
		spawnPos = vec3(0.0f, 3.0f, 0.0f);

		platforms.createLevel(1.0f, 5.5f, 5, 5);
		numPlatforms = 26;

		numWalls = 1;
		walls.push_back(Wall(vec3(-10.75, -1, -10), vec3(0, 15, 10), vec3(0, 1, 1)));

		vec3 finalPlat = platforms.getPlatformLocation(25);
		finalPlat.y += 1;
		currentWinDiamond.updateCenter(finalPlat);
	}
	else if (currentLevel == 9) {
		walls.clear();
		numWalls = 0;
		spawnPos = vec3(0.0f, 3.0f, 0.0f);

		platforms.createLevel(.5f, 6.0f, 3, 10);
		numPlatforms = 31;

		numWalls = 1;
		walls.push_back(Wall(vec3(-10.75, -1, -10), vec3(0, 15, 10), vec3(1, 1, 1)));

		vec3 finalPlat = platforms.getPlatformLocation(30);
		finalPlat.y += 1;
		currentWinDiamond.updateCenter(finalPlat);
	}
	else if (currentLevel == 10) {
		walls.clear();
		numWalls = 0;
		spawnPos = vec3(0.0f, 3.0f, 0.0f);

		platforms.createLevel(1.0f, 6.0f, 5, 10);
		numPlatforms = 51;

		vec3 finalPlat = platforms.getPlatformLocation(50);
		finalPlat.y += 1;
		currentWinDiamond.updateCenter(finalPlat);
	}
	else {
		currentLevel = 1;
		numLives = 3;
		loadLevel();
	}

	player.setPos(spawnPos, camMatrix);
	terrainUpdated = true;
}