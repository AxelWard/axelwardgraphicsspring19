#version 330

in vec2 position;

uniform mat4 ProjectionMatrix;
uniform mat4 ViewMatrix;

void main() {
  gl_Position = ProjectionMatrix * vec4(position.x, position.y, 0, 1);
}