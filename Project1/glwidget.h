#ifndef __GLWIDGET__INCLUDE__
#define __GLWIDGET__INCLUDE__

#include <QOpenGLWidget>
#include <QOpenGLFunctions_3_3_Core>
#include <QMouseEvent>
#include <glm/glm.hpp>
#include <vector>
#include <string>

#include "button.h"
#include "panel.h"

#define GLM_FORCE_RADIANS

using glm::vec2;
using glm::vec3;

class GLWidget : public QOpenGLWidget, protected QOpenGLFunctions_3_3_Core {
	Q_OBJECT

public:
	GLWidget(QWidget *parent = 0);
	~GLWidget();

	void sendPath(char*& filePath);

protected:
	void initializeGL();
	void resizeGL(int w, int h);
	void paintGL();

	void mousePressEvent(QMouseEvent *event);
	void mouseMoveEvent(QMouseEvent *event);
	void keyPressEvent(QKeyEvent *event);

	void openFile();
	void display(const QImage& img);

	void addSquare(int x, int y, vec3 color);
	void addCircle(int x, int y, vec3 color);
	void sizeUp();
	void sizeDown();
	void fill();
	void clear();
	void open();

	void createMenu();
	void renderMenu();
	void destroyMenu();

private:
	GLuint loadShaders(const char* vertf, const char* fragf);
	static const GLchar* readShader(const char* filename);

	GLuint vao;
	GLuint program;
	GLuint positionBuffer;
	GLuint colorBuffer;

	GLint projectionMatrixLoc;

	bool menuIsOpen;
	int drawMode;
	int num_squares;
	int num_circles;
	float size;
	std::string path;


	QImage image;
	QRect bounds;
	glm::mat4 ortho;

	std::vector<vec2> pts;
	std::vector<vec3> colors;

	Panel menuPanel;
};

#endif