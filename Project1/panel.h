#ifndef __PANEL__INCLUDE__
#define __PANEL__INCLUDE__

#include <vector>
#include <glm/glm.hpp>
#include "button.h"

using glm::vec2;

class Panel {

public:
	Panel();
	~Panel();

	void addButton(vec2 tl, int w, int h, vec3 color);
	void clearButtons();

	int mouseClick(int x, int y);

	int numButtons;

	vec2 getButtonStart(int index);
	int getButtonWidth(int index);
	int getButtonHeight(int index);
	vec3 getButtonColor(int index);

private:
	std::vector<Button> children;
};

#endif