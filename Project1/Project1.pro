HEADERS += glwidget.h panel.h button.h
SOURCES += glwidget.cpp main.cpp panel.cpp button.cpp

QT += opengl
CONFIG -= app_bundle
CONFIG += console
INCLUDEPATH += "../include"
INCLUDEPATH += "./glm/"
INCLUDEPATH += "./glm/gtc"
RESOURCES += \
    shaders.qrc