#include "panel.h"

#include <vector>
#include <glm/glm.hpp>
#include "button.h"

using glm::vec2;
using glm::vec3;

Panel::Panel() {
	numButtons = 0;
}

Panel::~Panel() {

}

void Panel::addButton(vec2 tl, int w, int h, vec3 color) {
	Button temp = Button(tl, w, h, color);

	children.push_back(temp);

	numButtons++;
}

void Panel::clearButtons() {
	children.clear();
	numButtons = 0;
}

int Panel::mouseClick(int x, int y) {
	
	for (int i = 0; i < numButtons; i++) {
		if (children[i].mouseClick(x, y)) {
			return(i);
		}
	}

	return(-1);
}

vec2 Panel::getButtonStart(int index) {
	return(children[index].topLeft);
}

int Panel::getButtonWidth(int index) {
	return(children[index].width);
}

int Panel::getButtonHeight(int index) {
	return(children[index].height);
}

vec3 Panel::getButtonColor(int index) {
	return(children[index].color);
}
