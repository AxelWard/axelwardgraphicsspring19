#include "button.h"

#include <glm/glm.hpp>

using glm::vec2;
using glm::vec3;

Button::Button(vec2 tl, int w, int h, vec3 inColor) {
	topLeft = tl;
	width = w;
	height = h;
	color = inColor;
}

Button::~Button() {

}

bool Button::mouseClick(int x, int y) {

	if((x > topLeft.x) && (x < topLeft.x + width)) {
		if ((y > topLeft.y) && (y < topLeft.y + height)) {
			return true;
		}
	}

	return false;
}