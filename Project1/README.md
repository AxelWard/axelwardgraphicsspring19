# Instructions

Press Space to open/close menu

Button Functions:

    White - Draw Circles

    Light Blue - Draw Squares

    Green - Size Up

    Dark Green - Size Down

    Blue - Fill using current size/shape

    Red - Clear

    Grey - Open File Prompt

