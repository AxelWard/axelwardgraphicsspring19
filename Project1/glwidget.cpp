#include "glwidget.h"
#include <iostream>
#include <math.h> 

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <QUrl>
#include <QFileDialog>
#include <QTextStream>
#include <QImage>
#include <QRect>
#include <QColor>
#include <QString>
#include <QPainter>

#include "panel.h"

using namespace std;

GLWidget::GLWidget(QWidget *parent) : QOpenGLWidget(parent) {
	num_squares = 0;
	num_circles = 0;

	drawMode = 0;

	menuIsOpen = false;

	size = 10;

	menuPanel = Panel();

	ortho = glm::ortho(0.0f, 640.0f, 480.0f, 0.0f);

	path = std::string("C:/Users/ajwea/Documents/IndependentStudy/axelwardgraphicsspring19/Project1/images/mountains.png");
	openFile();
}

GLWidget::~GLWidget() {
}

void GLWidget::sendPath(char*& filePath) {

	cout << filePath << endl;

	path = std::string(filePath);
	openFile();
}

void GLWidget::keyPressEvent(QKeyEvent *event) {
	switch (event->key()) {
	case Qt::Key_C:
		cout << "Drawing Circles" << endl;
		drawMode = 1;

		break;

	case Qt::Key_S:
		cout << "Drawing Squares" << endl;
		drawMode = 0;

		break;

	case Qt::Key_Space:

		if (menuIsOpen) {
			cout << "Closing Menu" << endl;
			destroyMenu();
			menuIsOpen = false;
		}
		else {
			cout << "Opening Menu" << endl;
			createMenu();
			menuIsOpen = true;
		}

		break;

	case Qt::Key_P:

		cout << "Projection Location: " << projectionMatrixLoc << endl << endl;

		cout << "Projection Matrix:" << endl << endl;
		cout << "------------" << endl;
		cout << ortho[0][0] << " ";
		cout << ortho[1][0] << " ";
		cout << ortho[2][0] << " ";
		cout << ortho[3][0] << endl;
		cout << "------------" << endl;
		cout << ortho[0][1] << " ";
		cout << ortho[1][1] << " ";
		cout << ortho[2][1] << " ";
		cout << ortho[3][1] << endl;
		cout << "------------" << endl;
		cout << ortho[0][2] << " ";
		cout << ortho[1][2] << " ";
		cout << ortho[2][2] << " ";
		cout << ortho[3][2] << endl;
		cout << "------------" << endl;
		cout << ortho[0][3] << " ";
		cout << ortho[1][3] << " ";
		cout << ortho[2][3] << " ";
		cout << ortho[3][3] << endl;
		cout << "------------" << endl << endl << endl;

		break;
	}

	glBindBuffer(GL_ARRAY_BUFFER, positionBuffer);
	glBufferData(GL_ARRAY_BUFFER, pts.size() * sizeof(vec2), &pts[0], GL_DYNAMIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, colorBuffer);
	glBufferData(GL_ARRAY_BUFFER, colors.size() * sizeof(vec3), &colors[0], GL_DYNAMIC_DRAW);

	update();
}

void GLWidget::mousePressEvent(QMouseEvent *event) {

	glm::vec3 color = glm::vec3(1, 1, 1);

	glm::vec2 temp = glm::vec2(0.0);
	temp.x = event->x();
	temp.y = event->y();
	
	if (menuIsOpen) {

		int clicked = menuPanel.mouseClick(temp.x, temp.y);

		if (clicked == 0) {
			cout << "Drawing Circles" << endl;
			drawMode = 1;
		}
		else if (clicked == 1) {
			cout << "Drawing Squares" << endl;
			drawMode = 0;
		}
		else if (clicked == 2) {
			sizeUp();
		}
		else if (clicked == 3) {
			sizeDown();
		}
		else if (clicked == 4) {
			menuIsOpen = false;
			destroyMenu();
			fill();
		}
		else if (clicked == 5) {
			menuIsOpen = false;
			destroyMenu();
			clear();
		}
		else if (clicked == 6) {
			menuIsOpen = false;
			destroyMenu();
			open();
		}
		else if (clicked == -1) {
			cout << "Button Not Setup Yet!" << endl;
		}
	}
	else if (temp.x < (image.width()) && temp.y < (image.height())) {
		QColor imgColor = image.pixelColor(temp.x, temp.y);
		color = glm::vec3(imgColor.red() / 255.0f, imgColor.green() / 255.0f, imgColor.blue() / 255.0f);

		if (drawMode == 0) {
			addSquare(temp.x, temp.y, color);
			num_squares++;
		}
		else if (drawMode == 1) {
			addCircle(temp.x, temp.y, color);
			num_circles++;
		}
	}

	glBindBuffer(GL_ARRAY_BUFFER, positionBuffer);
	glBufferData(GL_ARRAY_BUFFER, pts.size()*sizeof(vec2), &pts[0], GL_DYNAMIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, colorBuffer);
	glBufferData(GL_ARRAY_BUFFER, colors.size() * sizeof(vec3), &colors[0], GL_DYNAMIC_DRAW);

	update();
}

void GLWidget::mouseMoveEvent(QMouseEvent *event) {
	if (event->buttons() == Qt::LeftButton || event->buttons() == Qt::RightButton) {
		glm::vec3 color = glm::vec3(1, 1, 1);

		glm::vec2 temp = glm::vec2(0.0);
		temp.x = event->x();
		temp.y = event->y();

		if (temp.x < (image.width()) && temp.y < (image.height()) && !menuIsOpen) {
			QColor imgColor = image.pixelColor(temp.x, temp.y);
			color = glm::vec3(imgColor.red() / 255.0f, imgColor.green() / 255.0f, imgColor.blue() / 255.0f);

			if (drawMode == 0) {
				addSquare(temp.x, temp.y, color);
				num_squares++;
			}
			else if (drawMode == 1) {
				addCircle(temp.x, temp.y, color);
				num_circles++;
			}
		}
	}

	glBindBuffer(GL_ARRAY_BUFFER, positionBuffer);
	glBufferData(GL_ARRAY_BUFFER, pts.size() * sizeof(vec2), &pts[0], GL_DYNAMIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, colorBuffer);
	glBufferData(GL_ARRAY_BUFFER, colors.size() * sizeof(vec3), &colors[0], GL_DYNAMIC_DRAW);

	update();
}

void GLWidget::addSquare(int x, int y, glm::vec3 color) {
	pts.push_back(glm::vec2(x - size, y + size));
	colors.push_back(color);

	pts.push_back(glm::vec2(x + size, y + size));
	colors.push_back(color);

	pts.push_back(glm::vec2(x - size, y - size));
	colors.push_back(color);

	pts.push_back(glm::vec2(x - size, y - size));
	colors.push_back(color);

	pts.push_back(glm::vec2(x + size, y + size));
	colors.push_back(color);

	pts.push_back(glm::vec2(x + size, y - size));
	colors.push_back(color);
}

void GLWidget::addCircle(int x, int y, glm::vec3 color) {
	for (int angle = 0; angle < 360; angle += 20) {
		pts.push_back(glm::vec2(x, y));
		colors.push_back(color);

		pts.push_back(glm::vec2((float)(x + (size * sin(angle*(3.14159 / 180)))), (float)(y + (size * cos(angle*(3.14159 / 180))))));
		colors.push_back(color);

		pts.push_back(glm::vec2((float)(x + (size * sin((angle + 20) * (3.14159 / 180)))), (float)(y + (size * cos((angle + 20) * (3.14159 / 180))))));
		colors.push_back(color);
	}
}

void GLWidget::sizeUp() {
	if (size < 50) {
		size++;
		cout << "Increased Size" << endl;
	}
	else {
		cout << "Size is at the max" << endl;
	}
}

void GLWidget::sizeDown() {
	if (size > 1) {
		size--;
		cout << "Decreased Size" << endl;
	}
	else {
		cout << "Size is at the minimum" << endl;
	}
}

void GLWidget::fill() {
	int w = image.width();
	int h = image.height();

	glm::vec3 color = glm::vec3(1, 1, 1);

	for (int i = size; i < w; i += size*2) {
		for (int j = size; j < h; j += size*2) {
			QColor imgColor = image.pixelColor(i, j);
			color = glm::vec3(imgColor.red() / 255.0f, imgColor.green() / 255.0f, imgColor.blue() / 255.0f);

			if (drawMode == 0) {
				addSquare(i, j, color);
				num_squares++;
			}
			else if (drawMode == 1) {
				addCircle(i, j, color);
				num_circles++;
			}
		}
	}
}

void GLWidget::clear() {
	num_circles = 0;
	num_squares = 0;

	pts.clear();
	colors.clear();

	glBindBuffer(GL_ARRAY_BUFFER, positionBuffer);
	glBufferData(GL_ARRAY_BUFFER, 0, NULL, GL_DYNAMIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, colorBuffer);
	glBufferData(GL_ARRAY_BUFFER, 0, NULL, GL_DYNAMIC_DRAW);
}

void GLWidget::open() {
	cout << "Enter File Path: ";
	cin >> path;

	clear();

	openFile();
}

void GLWidget::initializeGL() {
	initializeOpenGLFunctions();

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	glGenBuffers(1, &positionBuffer);
	glGenBuffers(1, &colorBuffer);

	glBindBuffer(GL_ARRAY_BUFFER, positionBuffer);
	glBufferData(GL_ARRAY_BUFFER, 0, NULL, GL_DYNAMIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, colorBuffer);
	glBufferData(GL_ARRAY_BUFFER, 0, NULL, GL_DYNAMIC_DRAW);

	program = loadShaders(":/vert.glsl", ":/frag.glsl");

	glBindBuffer(GL_ARRAY_BUFFER, positionBuffer);
	GLint positionIndex = glGetAttribLocation(program, "position");
	glEnableVertexAttribArray(positionIndex);
	glVertexAttribPointer(positionIndex, 2, GL_FLOAT, GL_FALSE, 0, 0);

	glBindBuffer(GL_ARRAY_BUFFER, colorBuffer);
	GLint colorIndex = glGetAttribLocation(program, "colorIn");
	glEnableVertexAttribArray(colorIndex);
	glVertexAttribPointer(colorIndex, 3, GL_FLOAT, GL_FALSE, 0, 0);

	glUseProgram(program);
	projectionMatrixLoc = glGetUniformLocation(program, "ProjectionMatrix");

	glUniformMatrix4fv(projectionMatrixLoc, 1, GL_FALSE, glm::value_ptr(ortho));
}

void GLWidget::resizeGL(int w, int h) {
	glViewport(0, 0, w, h);

	ortho = glm::ortho(0.0f, (float)w, (float)h, 0.0f);
	
	glUseProgram(program);
	glUniformMatrix4fv(projectionMatrixLoc, 1, GL_FALSE, glm::value_ptr(ortho));
}

void GLWidget::paintGL() {

	glClear(GL_COLOR_BUFFER_BIT);

	glUseProgram(program);

	if (menuIsOpen) {
		glDrawArrays(GL_TRIANGLES, 0, ((num_squares * 6) + (num_circles * 54) + (menuPanel.numButtons * 6)));
	}
	else {
		glDrawArrays(GL_TRIANGLES, 0, ((num_squares * 6) + (num_circles * 54)));
	}
}

// Copied from LoadShaders.cpp in the the oglpg-8th-edition.zip
// file provided by the OpenGL Programming Guide, 8th edition.
const GLchar* GLWidget::readShader(const char* filename) {
#ifdef WIN32
	FILE* infile;
	fopen_s(&infile, filename, "rb");
#else
	FILE* infile = fopen(filename, "rb");
#endif // WIN32

	if (!infile) {
#ifdef _DEBUG
		std::cerr << "Unable to open file '" << filename << "'" << std::endl;
#endif /* DEBUG */
		return NULL;
	}

	fseek(infile, 0, SEEK_END);
	int len = ftell(infile);
	fseek(infile, 0, SEEK_SET);

	GLchar* source = new GLchar[len + 1];

	fread(source, 1, len, infile);
	fclose(infile);
	 
	source[len] = 0;

	return const_cast<const GLchar*>(source);
}

GLuint GLWidget::loadShaders(const char* vertf, const char* fragf) {
	GLuint program = glCreateProgram();

	// read vertex shader from Qt resource file
	QFile vertFile(vertf);
	vertFile.open(QFile::ReadOnly | QFile::Text);
	QString vertString;
	QTextStream vertStream(&vertFile);
	vertString.append(vertStream.readAll());
	std::string vertSTLString = vertString.toStdString();

	const GLchar* vertSource = vertSTLString.c_str();

	GLuint vertShader = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vertShader, 1, &vertSource, NULL);
	glCompileShader(vertShader);
	{
		GLint compiled;
		glGetShaderiv(vertShader, GL_COMPILE_STATUS, &compiled);
		if (!compiled) {
			GLsizei len;
			glGetShaderiv(vertShader, GL_INFO_LOG_LENGTH, &len);

			GLchar* log = new GLchar[len + 1];
			glGetShaderInfoLog(vertShader, len, &len, log);
			std::cout << "Shader compilation failed: " << log << std::endl;
			delete[] log;
		}
	}
	glAttachShader(program, vertShader);

	// read fragment shader from Qt resource file
	QFile fragFile(fragf);
	fragFile.open(QFile::ReadOnly | QFile::Text);
	QString fragString;
	QTextStream fragStream(&fragFile);
	fragString.append(fragStream.readAll());
	std::string fragSTLString = fragString.toStdString();

	const GLchar* fragSource = fragSTLString.c_str();

	GLuint fragShader = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragShader, 1, &fragSource, NULL);
	glCompileShader(fragShader);
	{
		GLint compiled;
		glGetShaderiv(fragShader, GL_COMPILE_STATUS, &compiled);
		if (!compiled) {
			GLsizei len;
			glGetShaderiv(fragShader, GL_INFO_LOG_LENGTH, &len);

			GLchar* log = new GLchar[len + 1];
			glGetShaderInfoLog(fragShader, len, &len, log);
			std::cerr << "Shader compilation failed: " << log << std::endl;
			delete[] log;
		}
	}
	glAttachShader(program, fragShader);

	glLinkProgram(program);
	{
		GLint linked;
		glGetProgramiv(program, GL_LINK_STATUS, &linked);
		if (!linked) {
			GLsizei len;
			glGetProgramiv(program, GL_INFO_LOG_LENGTH, &len);

			GLchar* log = new GLchar[len + 1];
			glGetProgramInfoLog(program, len, &len, log);
			std::cout << "Shader linker failed: " << log << std::endl;
			delete[] log;
		}
	}

	return program;
}

void GLWidget::createMenu() {

	menuPanel.addButton(vec2(0, 0), 50, 50, vec3(1, 1, 1));       // draw circles
	menuPanel.addButton(vec2(0, 60), 50, 50, vec3(.5f, .5f, 1));    // draw squares
	menuPanel.addButton(vec2(100, 0), 50, 50, vec3(0, 1, 0));     // size up
	menuPanel.addButton(vec2(100, 60), 50, 50, vec3(0, .5f, 0));  // size down
	menuPanel.addButton(vec2(0, 125), 150, 25, vec3(0, 0, 1));    // fill
	menuPanel.addButton(vec2(0, 160), 25, 25, vec3(1, 0, 0));     // clear
	menuPanel.addButton(vec2(125, 160), 25, 25, vec3(.1f, .1f, .1f)); // Open File

	renderMenu();
}

void GLWidget::renderMenu() {
	int num_buttons = menuPanel.numButtons;

	for (int i = 0; i < num_buttons; i++) {

		vec2 start = menuPanel.getButtonStart(i);
		int w = menuPanel.getButtonWidth(i);
		int h = menuPanel.getButtonHeight(i);
		vec3 color = menuPanel.getButtonColor(i);

		pts.push_back(start);
		colors.push_back(color);

		pts.push_back(vec2(start.x, start.y + h));
		colors.push_back(color);

		pts.push_back(vec2(start.x + w, start.y));
		colors.push_back(color);

		pts.push_back(vec2(start.x, start.y + h));
		colors.push_back(color);

		pts.push_back(vec2(start.x + w, start.y + h));
		colors.push_back(color);

		pts.push_back(vec2(start.x + w, start.y));
		colors.push_back(color);
	}

	update();
}

void GLWidget::destroyMenu() {
	int num_buttons = menuPanel.numButtons;

	for (int i = 0; i < (num_buttons * 6); i++) {
		pts.pop_back();
		colors.pop_back();
	}

	menuPanel.clearButtons();
}

void GLWidget::openFile() {

	//QString url;

	//cout << "Enter image path (i.e C:/Path/image.png): " << endl;
	//cin << url;
	QString fpath = QString::fromStdString(path);
	QImage img(fpath);

	display(img);

}

void GLWidget::display(const QImage& img)
{
	image = img;

	bounds = image.rect();

	update();
}