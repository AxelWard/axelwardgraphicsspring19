#ifndef __BUTTON__INCLUDE__
#define __BUTTON__INCLUDE__

#include <glm/glm.hpp>

using glm::vec2;
using glm::vec3;

class Button {

public:
	Button(vec2 tl, int w, int h, vec3 color);
	~Button();

	bool mouseClick(int x, int y);

	vec2 topLeft;
	vec3 color;

	int width;
	int height;
};

#endif