#include "glwidget.h"
#include <iostream>
#include <QFile>
#include <QTextStream>

using namespace std;

GLWidget::GLWidget(QWidget *parent) : QOpenGLWidget(parent) {
}

GLWidget::~GLWidget() {
}

void GLWidget::initializeGL() {
	initializeOpenGLFunctions();

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	// Create a new Vertex Array Object on the GPU which
	// saves the attribute layout of our vertices.
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	float x1, x2, x3, y1, y2, y3;
	float r1, r2, r3, g1, g2, g3, b1, b2, b3;
	char colon, comma;

	cout << "Input 3 X,Y RGB sets as X,Y:R,G,B with the RGB values ranging from 0 to 1" << endl << "Set 1: ";
	cin >> x1 >> comma >> y1 >> colon >> r1 >> comma >> g1 >> comma >> b1;

	Point point1(x1, y1);
	Point nPoint1 = GLWidget::w2nd(point1);

	Color c1(r1, g1, b1);

	cout << "Set 2: ";
	cin >> x2 >> comma >> y2 >> colon >> r2 >> comma >> g2 >> comma >> b2;

	Point point2(x2, y2);
	Point nPoint2 = GLWidget::w2nd(point2);

	Color c2(r2, g2, b2);

	cout << "Set 3: ";
	cin >> x3 >> comma >> y3 >> colon >> r3 >> comma >> g3 >> comma >> b3;

	Point point3(x3, y3);
	Point nPoint3 = GLWidget::w2nd(point3);

	Color c3(r3, g3, b3);

	// position data for a single triangle
	Point pts[3] = {
		nPoint1,
		nPoint2,
		nPoint3
	};

	Color colors[3] = {
		c1,
		c2,
		c3
	};

	// Create a buffer on the GPU for position data
	GLuint positionBuffer;
	glGenBuffers(1, &positionBuffer);

	// Upload the position data to the GPU, storing
	// it in the buffer we just created.
	glBindBuffer(GL_ARRAY_BUFFER, positionBuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(pts), pts, GL_STATIC_DRAW);

	
	GLuint colorBuffer;
	glGenBuffers(1, &colorBuffer);

	glBindBuffer(GL_ARRAY_BUFFER, colorBuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(colors), colors, GL_STATIC_DRAW);
	

	// Load our vertex and fragment shaders into a program object
	// on the GPU
	program = loadShaders(":/vert.glsl", ":/frag.glsl");

	// Bind the attribute "position" (defined in our vertex shader)
	// to the currently bound buffer object, which contains our
	// position data for a single triangle. This information 
	// is stored in our vertex array object.
	glBindBuffer(GL_ARRAY_BUFFER, positionBuffer);
	GLint positionIndex = glGetAttribLocation(program, "position");
	glEnableVertexAttribArray(positionIndex);
	glVertexAttribPointer(positionIndex, 2, GL_FLOAT, GL_FALSE, 0, 0);
	
	glBindBuffer(GL_ARRAY_BUFFER, colorBuffer);
	GLint colorIndex = glGetAttribLocation(program, "color_in");
	glEnableVertexAttribArray(colorIndex);
	glVertexAttribPointer(colorIndex, 3, GL_FLOAT, GL_FALSE, 0, 0);
	
}

void GLWidget::resizeGL(int w, int h) {
	glViewport(0, 0, w, h);
}

void GLWidget::paintGL() {
	glClear(GL_COLOR_BUFFER_BIT);
	glUseProgram(program);
	glBindVertexArray(vao);
	glDrawArrays(GL_TRIANGLES, 0, 3);
}

Point GLWidget::w2nd(Point pt_w) {
	/* convert pt_w to normalized device coordinates */
	/* use this method to convert your input coordinates to
	normalized device coordinates */

	float normX = (-1.f + (pt_w.x * (2.f / 640)));
	float normY = (1.f - (pt_w.y * (2.f / 480)));

	Point pt_n(normX, normY);

	return pt_n;
}

GLuint GLWidget::loadShaders(const char* vertf, const char* fragf) {
	GLuint program = glCreateProgram();

	// read vertex shader from Qt resource file
	QFile vertFile(vertf);
	vertFile.open(QFile::ReadOnly | QFile::Text);
	QString vertString;
	QTextStream vertStream(&vertFile);
	vertString.append(vertStream.readAll());
	std::string vertSTLString = vertString.toStdString();

	const GLchar* vertSource = vertSTLString.c_str();

	GLuint vertShader = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vertShader, 1, &vertSource, NULL);
	glCompileShader(vertShader);
	{
		GLint compiled;
		glGetShaderiv(vertShader, GL_COMPILE_STATUS, &compiled);
		if (!compiled) {
			GLsizei len;
			glGetShaderiv(vertShader, GL_INFO_LOG_LENGTH, &len);

			GLchar* log = new GLchar[len + 1];
			glGetShaderInfoLog(vertShader, len, &len, log);
			std::cout << "Shader compilation failed: " << log << std::endl;
			delete[] log;
		}
	}
	glAttachShader(program, vertShader);

	// read fragment shader from Qt resource file
	QFile fragFile(fragf);
	fragFile.open(QFile::ReadOnly | QFile::Text);
	QString fragString;
	QTextStream fragStream(&fragFile);
	fragString.append(fragStream.readAll());
	std::string fragSTLString = fragString.toStdString();

	const GLchar* fragSource = fragSTLString.c_str();

	GLuint fragShader = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragShader, 1, &fragSource, NULL);
	glCompileShader(fragShader);
	{
		GLint compiled;
		glGetShaderiv(fragShader, GL_COMPILE_STATUS, &compiled);
		if (!compiled) {
			GLsizei len;
			glGetShaderiv(fragShader, GL_INFO_LOG_LENGTH, &len);

			GLchar* log = new GLchar[len + 1];
			glGetShaderInfoLog(fragShader, len, &len, log);
			std::cerr << "Shader compilation failed: " << log << std::endl;
			delete[] log;
		}
	}
	glAttachShader(program, fragShader);

	glLinkProgram(program);
	{
		GLint linked;
		glGetProgramiv(program, GL_LINK_STATUS, &linked);
		if (!linked) {
			GLsizei len;
			glGetProgramiv(program, GL_INFO_LOG_LENGTH, &len);

			GLchar* log = new GLchar[len + 1];
			glGetProgramInfoLog(program, len, &len, log);
			std::cout << "Shader linker failed: " << log << std::endl;
			delete[] log;
		}
	}

	return program;
}