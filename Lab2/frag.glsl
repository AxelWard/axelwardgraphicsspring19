#version 330

in vec3 color;

out vec4 color_out;

void main() {
  color_out = vec4(color.x, color.y, color.z, 1);
}