#version 330

in vec2 position;
in vec3 color_in;

out vec3 color;

void main() {
  gl_Position = vec4(position.x, position.y, 0, 1);
  color = vec3(color_in.x, color_in.y, color_in.z);
}